.. Tensorflow Guide documentation master file, created by
   sphinx-quickstart on Fri Dec  7 08:42:50 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Ultra96 Guide
=============

.. toctree::
    :maxdepth: 3
    :numbered:
    :includehidden:
    :caption: Contents:

    ultra96/helloworld
    ultra96/fir_pynq
    ultra96/vitis
    ultra96/zynq_fir
    ultra96/hls_zynq
    ultra96/fir_vivado
    ultra96/xmc_cardano

