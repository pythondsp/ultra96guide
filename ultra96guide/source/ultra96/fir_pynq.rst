FIR filter with Pynq
********************


Introduction
============

In this page, we will compare the speed of the FIR filter speed of Vivado's FIR compiler with Numpy's FIR. 

Board: Ultra96 V1

Vivado: 2019.1

Pyqn: 2.4



Vivado Project
==============

* Create the FIR project and generate the bitstream using below tcl script (or build it manually to generate the below figure), 


.. code-block:: text

    # vivado 2019.1
    # board Ultra96

    create_project pynq_fir ./pynq_fir -part xczu3eg-sbva484-1-e -force
    set_property board_part em.avnet.com:ultra96:part0:1.2 [current_project]

    # create block design
    create_bd_design "fir1"
    update_compile_order -fileset sources_1
     
    # add zynq
    create_bd_cell -type ip -vlnv xilinx.com:ip:zynq_ultra_ps_e:3.3 zynq_ultra_ps_e_0
    apply_bd_automation -rule xilinx.com:bd_rule:zynq_ultra_ps_e -config {apply_board_preset "1" }  [get_bd_cells zynq_ultra_ps_e_0]
     
    # add FIR
    create_bd_cell -type ip -vlnv xilinx.com:ip:fir_compiler:7.2 fir_compiler_0
    set_property -dict [list CONFIG.Data_Width.VALUE_SRC USER] [get_bd_cells fir_compiler_0]
    set_property -dict [list CONFIG.Sample_Frequency {100} CONFIG.Clock_Frequency {100} CONFIG.Data_Width {32} CONFIG.Output_Rounding_Mode {Non_Symmetric_Rounding_Up} CONFIG.Output_Width {32} CONFIG.DATA_Has_TLAST {Packet_Framing} CONFIG.M_DATA_Has_TREADY {true} CONFIG.Coefficient_Width {16} CONFIG.Coefficient_Structure {Inferred} CONFIG.Filter_Architecture {Systolic_Multiply_Accumulate} CONFIG.ColumnConfig {11}] [get_bd_cells fir_compiler_0] 
    set_property -dict [list CONFIG.CoefficientVector {-255,-260,-312,-288,-144,153,616,1233,1963,2739,3474,4081,4481,4620,4481,4081,3474,2739,1963,1233,616,153,-144,-288,-312,-260,-255} CONFIG.Coefficient_Sets {1} CONFIG.Coefficient_Sign {Signed} CONFIG.Quantization {Integer_Coefficients} CONFIG.Coefficient_Width {16} CONFIG.Coefficient_Fractional_Bits {0} CONFIG.Coefficient_Structure {Inferred} CONFIG.Data_Width {32} CONFIG.Output_Width {32} CONFIG.ColumnConfig {14}] [get_bd_cells fir_compiler_0]
     
     
     
    # AXI DMA
    create_bd_cell -type ip -vlnv xilinx.com:ip:axi_dma:7.1 axi_dma_0
    set_property -dict [list CONFIG.c_include_sg {0} CONFIG.c_sg_length_width {23} CONFIG.c_sg_include_stscntrl_strm {0}] [get_bd_cells axi_dma_0]
     
     
    # connection
    connect_bd_intf_net [get_bd_intf_pins fir_compiler_0/M_AXIS_DATA] [get_bd_intf_pins axi_dma_0/S_AXIS_S2MM]
    connect_bd_intf_net [get_bd_intf_pins axi_dma_0/M_AXIS_MM2S] [get_bd_intf_pins fir_compiler_0/S_AXIS_DATA]
     
     
    # add HP Slave
    set_property -dict [list CONFIG.PSU__USE__S_AXI_GP2 {1}] [get_bd_cells zynq_ultra_ps_e_0]
     
    # connection automation
    apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config { Clk_master {Auto} Clk_slave {Auto} Clk_xbar {Auto} Master {/axi_dma_0/M_AXI_MM2S} Slave {/zynq_ultra_ps_e_0/S_AXI_HP0_FPD} intc_ip {Auto} master_apm {0}}  [get_bd_intf_pins zynq_ultra_ps_e_0/S_AXI_HP0_FPD]
    apply_bd_automation -rule xilinx.com:bd_rule:clkrst -config {Clk "/zynq_ultra_ps_e_0/pl_clk0 (100 MHz)" }  [get_bd_pins fir_compiler_0/aclk]
    apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config { Clk_master {Auto} Clk_slave {Auto} Clk_xbar {Auto} Master {/zynq_ultra_ps_e_0/M_AXI_HPM0_FPD} Slave {/axi_dma_0/S_AXI_LITE} intc_ip {New AXI Interconnect} master_apm {0}}  [get_bd_intf_pins axi_dma_0/S_AXI_LITE]
     
    apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config { Clk_master {Auto} Clk_slave {/zynq_ultra_ps_e_0/pl_clk0 (100 MHz)} Clk_xbar {/zynq_ultra_ps_e_0/pl_clk0 (100 MHz)} Master {/zynq_ultra_ps_e_0/M_AXI_HPM1_FPD} Slave {/axi_dma_0/S_AXI_LITE} intc_ip {/ps8_0_axi_periph} master_apm {0}}  [get_bd_intf_pins zynq_ultra_ps_e_0/M_AXI_HPM1_FPD]
     
    apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config { Clk_master {/zynq_ultra_ps_e_0/pl_clk0 (100 MHz)} Clk_slave {/zynq_ultra_ps_e_0/pl_clk0 (100 MHz)} Clk_xbar {/zynq_ultra_ps_e_0/pl_clk0 (100 MHz)} Master {/axi_dma_0/M_AXI_S2MM} Slave {/zynq_ultra_ps_e_0/S_AXI_HP0_FPD} intc_ip {/axi_smc} master_apm {0}}  [get_bd_intf_pins axi_dma_0/M_AXI_S2MM]
     
    # rename
    set_property name fir_dma [get_bd_cells axi_dma_0]
    set_property name fir [get_bd_cells fir_compiler_0]
    group_bd_cells filter [get_bd_cells fir] [get_bd_cells fir_dma]
     
     
     
    regenerate_bd_layout
    validate_bd_design
    save_bd_design
     
     
    # create wrapper
    make_wrapper -files [get_files ./pynq_fir/pynq_fir.srcs/sources_1/bd/fir1/fir1.bd] -top
    add_files -norecurse ./pynq_fir/pynq_fir.srcs/sources_1/bd/fir1/hdl/fir1_wrapper.v
     
     
    launch_runs impl_1 -to_step write_bitstream -jobs 40


* Design in :numref:`fig_pynq1` will be created with above script, 

.. _`fig_pynq1`:

.. figure:: img/pynq/pynq1.png
    :width: 100%

    FIR compiler with Zynq


* In above step, the .bit will be saved at location "<project_name>/<project_name>.runs/impl_1/"
* Next, go to Export -> Export Block design -> save the .tcl file at desired location. 
* Copy the .bit file and .tcl file at the desired location; and rename them e.g. fir1.bit and fir1.tcl. The name should be same for both the files which is the requirement from Pynq. 


Prepare SD card
===============


* Connect the JTAG cable between FPGA to Laptop.
* Connect Laptop to FPGA with ethernet port (i.e. MicroUSB port of Ultra96). 
* Copy the pynq image to sd card and boot the FPGA. After the successful boot, check the IP address of board using Teraterm as shown in :numref:`fig_pynq2`, 

.. _`fig_pynq2`:

.. figure:: img/pynq/pynq2.png
    :width: 75%

    Check IP address of FPGA on TeraTerm

* Now, map the the Pynq drive to Laptop using this IP address

.. _`fig_pynq3`:

.. figure:: img/pynq/pynq3.png
    :width: 75%

    Map the the Pynq drive to Laptop

* Now copy the 'fir1.tcl' and 'fir1.bit' file to the Pynq drive i.e. "Q:/pynq/overlays/fir1/<both files here>"


Execute design in Jupyter Notebook
==================================


* Open the Jupyter notebook in web-browser using the IP address of the board i.e. "http://10.175.35.49:9090/" 
* Next, create a new Python3 notebook; and run the below commands, 


Generate Samples
----------------


.. code-block:: python

    %matplotlib notebook
    import matplotlib.pyplot as plt
     
    # settings for plots
    def plot_to_notebook(time_sec,in_signal,n_samples,out_signal=None):
        plt.figure()
        plt.subplot(1, 1, 1)
        plt.xlabel('Time (usec)')
        plt.grid()
        plt.plot(time_sec[:n_samples]*1e6,in_signal[:n_samples],'y-',label='Input signal')
        if out_signal is not None:
            plt.plot(time_sec[:n_samples]*1e6,out_signal[:n_samples],'g-',linewidth=2,label='FIR output')
        plt.legend()
     
     
    # generate and plot samples
    import numpy as np
     
    # Total time
    T = 0.002
    # Sampling frequency
    fs = 100e6
    # Number of samples
    n = int(T * fs)
    # Time vector in seconds
    t = np.linspace(0, T, n, endpoint=False)
    # Samples of the signal
    samples = 10000*np.sin(0.2e6*2*np.pi*t) + 1500*np.cos(46e6*2*np.pi*t) + 2000*np.sin(12e6*2*np.pi*t)
    # Convert samples to 32-bit integers
    samples = samples.astype(np.int32)
    print('Number of samples: ',len(samples))
     
    # Plot signal to the notebook
    plot_to_notebook(t,samples,1000)


* :numref:`fig_pynq4` is the output for above code, 


.. _`fig_pynq4`:

.. figure:: img/pynq/pynq4.png
    :width: 75%

    Sample data i.e. sum of different sine waves



Scipy FIR
---------


Now, implement the FIR filter using Scipy library, 

.. code-block:: python

    from scipy.signal import lfilter
     
    coeffs = [-255,-260,-312,-288,-144,153,616,1233,1963,2739,3474,4081,4481,4620,4481,4081,3474,2739,1963,1233,616,153,-144,-288,-312,-260,-255]
     
    import time
    start_time = time.time()
    sw_fir_output = lfilter(coeffs,70e3,samples)
    stop_time = time.time()
    sw_exec_time = stop_time - start_time
    print('Software FIR execution time: ',sw_exec_time)
     
    # Plot the result to notebook
    plot_to_notebook(t,samples,1000,out_signal=sw_fir_output)


* :numref:`fig_pynq5` is the output of above code, 


.. _`fig_pynq5`:

.. figure:: img/pynq/pynq5.png
    :width: 75%

    Filtered sine wave with FIR filter (software version)



Vivado FIR
----------

Now, run the Vivado FIR filter, 

.. code-block:: python

    from pynq import Overlay
    import pynq.lib.dma
     
    # Load the overlay
    overlay = Overlay('/home/xilinx/pynq/overlays/fir1/fir1.bit')
     
    # Load the FIR DMA
    dma = overlay.filter.fir_dma

    from pynq import Xlnk
    import numpy as np
     
    # Allocate buffers for the input and output signals
    xlnk = Xlnk()
    in_buffer = xlnk.cma_array(shape=(n,), dtype=np.int32)
    out_buffer = xlnk.cma_array(shape=(n,), dtype=np.int32)
     
    # Copy the samples to the in_buffer
    np.copyto(in_buffer,samples)
     
    # Trigger the DMA transfer and wait for the result
    import time
    start_time = time.time()
    dma.sendchannel.transfer(in_buffer)
    dma.recvchannel.transfer(out_buffer)
    dma.sendchannel.wait()
    dma.recvchannel.wait()
    stop_time = time.time()
    hw_exec_time = stop_time-start_time
    print('Hardware FIR execution time: ',hw_exec_time)
    print('Hardware acceleration factor: ',sw_exec_time / hw_exec_time)
     
    # Plot to the notebook
    plot_to_notebook(t,samples,1000,out_signal=out_buffer)
     
    # Free the buffers
    in_buffer.close()
    out_buffer.close()


* :numref:`fig_pynq6` is the output for above code which shows acceleration factor as 12.27 using Vivado FIR.

.. _`fig_pynq6`:

.. figure:: img/pynq/pynq6.png
    :width: 75%

    Filtered sine wave with FIR filter (Hardware version)



.. Driver for Vivado FIR
.. ---------------------


.. * It's better to provide the driver to end user (instead of full code), so that the design can be easily used.  

.. .. code-block:: python

..     from pynq import DefaultHierarchy
     
..     class FirDriver(DefaultHierarchy):
..         def __init__(self, description):
..             super().__init__(description)
     
..         def fir_filter(self, data):
..             with xlnk.cma_array(shape=(len(data),), dtype=np.int32) as in_buffer,\
..                  xlnk.cma_array(shape=(len(data),), dtype=np.int32) as out_buffer:
..                 np.copyto(in_buffer,data)
..                 self.fir_dma.sendchannel.transfer(in_buffer)
..                 self.fir_dma.recvchannel.transfer(out_buffer)
..                 self.fir_dma.sendchannel.wait()
..                 self.fir_dma.recvchannel.wait()
..                 result = out_buffer.copy()
..             return result
     
..         @staticmethod
..         def checkhierarchy(description):
..             if 'fir' in description['ip'] \
..                and 'fir_dma' in description['ip']:
..                 return True
..             return False



..     from pynq import Overlay
..     import pynq.lib.dma
     
..     overlay = Overlay('/home/xilinx/pynq/overlays/fir1/fir1.bit')
     
..     # Run the hardware FIR and measure the runtime
..     start_time = time.time()
..     # result = overlay.filter.fir_filter(samples)
..     stop_time = time.time()
..     print('Hardware FIR execution time (with driver): ',stop_time-start_time)
     
..     # Plot to the notebook
..     plot_to_notebook(t,samples,1000,out_signal=out_buffer)


.. * :numref:`fig_pynq7` is the output for above code, 


.. .. _`fig_pynq7`:

.. .. figure:: img/pynq/pynq7.png
..     :width: 75%

..     Filtered sine wave with FIR filter (Hardware version with driver)


.. Conclusion
.. ==========


.. In this chapter, we implemented the FIR filter using Scipy libary and Vivado. Also, we wrote a driver code in Python for the Vivado design so that end-user can use the design easily. 