Zynq with DMA
*************


FIFO with DMA
=============

Vivado
------

.. code-block:: tcl

    # Vivado 2019.2
    # ultra96 

    # Create project 
    create_project dma_fifo ./dma_fifo -part xczu3eg-sbva484-1-e
    set_property board_part em.avnet.com:ultra96:part0:1.2 [current_project]


    create_bd_design "fifo_dma" 
    update_compile_order -fileset sources_1 

    # zynq
    create_bd_cell -type ip -vlnv xilinx.com:ip:zynq_ultra_ps_e:3.3 zynq_ultra_ps_e_0
    apply_bd_automation -rule xilinx.com:bd_rule:zynq_ultra_ps_e -config {apply_board_preset "1" }  [get_bd_cells zynq_ultra_ps_e_0]


    # AXI stream data fifo
    create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_data_fifo_0


    # AXI DMA
    create_bd_cell -type ip -vlnv xilinx.com:ip:axi_dma:7.1 axi_dma_0
    set_property -dict [list CONFIG.c_include_sg {0} CONFIG.c_sg_include_stscntrl_strm {0}] [get_bd_cells axi_dma_0]

    # AXI interconnect
    create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 axi_interconnect_0
    set_property -dict [list CONFIG.NUM_MI {1}] [get_bd_cells axi_interconnect_0]
    set_property -dict [list CONFIG.NUM_SI {2} CONFIG.NUM_MI {1}] [get_bd_cells axi_interconnect_0]


    # connection
    connect_bd_intf_net [get_bd_intf_pins axi_dma_0/M_AXIS_MM2S] [get_bd_intf_pins axis_data_fifo_0/S_AXIS]
    connect_bd_intf_net [get_bd_intf_pins axi_dma_0/M_AXI_MM2S] -boundary_type upper [get_bd_intf_pins axi_interconnect_0/S00_AXI]
    connect_bd_intf_net [get_bd_intf_pins axi_dma_0/M_AXI_S2MM] -boundary_type upper [get_bd_intf_pins axi_interconnect_0/S01_AXI]
    apply_bd_automation -rule xilinx.com:bd_rule:clkrst -config { Clk {/zynq_ultra_ps_e_0/pl_clk0 (100 MHz)} Freq {100} Ref_Clk0 {} Ref_Clk1 {} Ref_Clk2 {}}  [get_bd_pins axi_interconnect_0/S01_ACLK]
    apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config { Clk_master {Auto} Clk_slave {/zynq_ultra_ps_e_0/pl_clk0 (100 MHz)} Clk_xbar {/zynq_ultra_ps_e_0/pl_clk0 (100 MHz)} Master {/zynq_ultra_ps_e_0/M_AXI_HPM1_FPD} Slave {/axi_dma_0/S_AXI_LITE} ddr_seg {Auto} intc_ip {/ps8_0_axi_periph} master_apm {0}}  [get_bd_intf_pins zynq_ultra_ps_e_0/M_AXI_HPM1_FPD]
    connect_bd_net [get_bd_pins axi_interconnect_0/M00_ACLK] [get_bd_pins zynq_ultra_ps_e_0/pl_clk0]
    connect_bd_net [get_bd_pins axi_interconnect_0/M00_ARESETN] [get_bd_pins rst_ps8_0_100M/peripheral_aresetn]
    connect_bd_intf_net [get_bd_intf_pins axis_data_fifo_0/M_AXIS] [get_bd_intf_pins axi_dma_0/S_AXIS_S2MM]
    set_property -dict [list CONFIG.PSU__USE__S_AXI_GP0 {1}] [get_bd_cells zynq_ultra_ps_e_0]
    apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config { Clk_master {/zynq_ultra_ps_e_0/pl_clk0 (100 MHz)} Clk_slave {Auto} Clk_xbar {/zynq_ultra_ps_e_0/pl_clk0 (100 MHz)} Master {/axi_dma_0/M_AXI_MM2S} Slave {/zynq_ultra_ps_e_0/S_AXI_HPC0_FPD} ddr_seg {Auto} intc_ip {/axi_interconnect_0} master_apm {0}}  [get_bd_intf_pins zynq_ultra_ps_e_0/S_AXI_HPC0_FPD]


    assign_bd_address 
    regenerate_bd_layout
    validate_bd_design
    save_bd_design



    make_wrapper -files [get_files ./dma_fifo/dma_fifo.srcs/sources_1/bd/fifo_dma/fifo_dma.bd] -top
    add_files -norecurse ./dma_fifo/dma_fifo.srcs/sources_1/bd/fifo_dma/hdl/fifo_dma_wrapper.v

    launch_runs impl_1 -to_step write_bitstream -jobs 56 


* Design in :numref:`fig_zynq_fir2` will be created with above script, 

.. _`fig_zynq_fir2`:

.. figure:: img/zynq_fir/zynq_fir2.jpg
    :width: 100%

    Generated design for FIFO and DMA 


Vitis
-----


.. code-block:: c

    #include <stdio.h>
    #include "xaxidma.h"
    #include "xparameters.h"
    #include "xil_printf.h"


    /******************** Constant Definitions **********************************/

    // Device hardware build related constants.
    #define DMA_DEV_ID      XPAR_AXIDMA_0_DEVICE_ID

    #define MEM_BASE_ADDR       0x01000000
    #define TX_BUFFER_BASE      (MEM_BASE_ADDR + 0x00100000)
    #define RX_BUFFER_BASE      (MEM_BASE_ADDR + 0x00300000)
    #define RX_BUFFER_HIGH      (MEM_BASE_ADDR + 0x004FFFFF)

    #define NUM_TABLE    0x5  // number to print the table
    #define NUM_ITER     0xa // print table upto NUM_ITER



    // Device instance definitions
    XAxiDma AxiDma;


    int main()
    {
        int Status;

        xil_printf("\r\n--- Entering main() --- \r\n");

        XAxiDma_Config *CfgPtr;
        int Index;
        u8 *TxBufferPtr;
        u8 *RxBufferPtr;
        u8 Value;

        TxBufferPtr = (u8 *)TX_BUFFER_BASE ;
        RxBufferPtr = (u8 *)RX_BUFFER_BASE;

        /* Initialize the XAxiDma device.
         */
        CfgPtr = XAxiDma_LookupConfig(DMA_DEV_ID);
        if (!CfgPtr) {
            xil_printf("No config found for %d\r\n", DMA_DEV_ID);
            return XST_FAILURE;
        }

        Status = XAxiDma_CfgInitialize(&AxiDma, CfgPtr);
        if (Status != XST_SUCCESS) {
            xil_printf("Initialization failed %d\r\n", Status);
            return XST_FAILURE;
        }

        if(XAxiDma_HasSg(&AxiDma)){
            xil_printf("Device configured as SG mode \r\n");
            return XST_FAILURE;
        }

        /* Disable interrupts, we use polling mode
         */
        XAxiDma_IntrDisable(&AxiDma, XAXIDMA_IRQ_ALL_MASK,
                            XAXIDMA_DEVICE_TO_DMA);
        XAxiDma_IntrDisable(&AxiDma, XAXIDMA_IRQ_ALL_MASK,
                            XAXIDMA_DMA_TO_DEVICE);

        // flush the RX buffer
        Xil_DCacheFlushRange((UINTPTR)RxBufferPtr, NUM_ITER);

        printf("################# Tx data ######################\n");
        for(Index = 0; Index < NUM_ITER; Index ++) {
                Value = NUM_TABLE*Index;
                TxBufferPtr[Index] = Value;
                printf("Value[%d]: %d\n", Index, Value);

        }

        Status = XAxiDma_SimpleTransfer(&AxiDma,(UINTPTR) RxBufferPtr,
                            NUM_ITER, XAXIDMA_DEVICE_TO_DMA);

        if (Status != XST_SUCCESS) {
            return XST_FAILURE;
        }

        Status = XAxiDma_SimpleTransfer(&AxiDma,(UINTPTR) TxBufferPtr,
                    NUM_ITER, XAXIDMA_DMA_TO_DEVICE);

        if (Status != XST_SUCCESS) {
            return XST_FAILURE;
        }

        // flush the TX buffer
        Xil_DCacheFlushRange((UINTPTR)TxBufferPtr, NUM_ITER);



        while ((XAxiDma_Busy(&AxiDma,XAXIDMA_DEVICE_TO_DMA)) ||
                   (XAxiDma_Busy(&AxiDma,XAXIDMA_DMA_TO_DEVICE))) {
               }

        printf("\n################# Rx data ######################\n");
        for(Index = 0; Index < NUM_ITER; Index ++) {
                printf("Value[%d]: %d\n", Index, RxBufferPtr[Index]);
        }

        return XST_SUCCESS;

    }



Result
------

.. code-block:: text

    ################# Tx data ######################
    Value[0]: 0
    Value[1]: 5
    Value[2]: 10
    Value[3]: 15
    Value[4]: 20
    Value[5]: 25
    Value[6]: 30
    Value[7]: 35
    Value[8]: 40
    Value[9]: 45

    ################# Rx data ######################
    Value[0]: 0
    Value[1]: 5
    Value[2]: 10
    Value[3]: 15
    Value[4]: 20
    Value[5]: 25
    Value[6]: 30
    Value[7]: 35
    Value[8]: 40
    Value[9]: 45




FIR with DMA
============


Vivado project
--------------

.. code-block:: text

    # Vivado 2019.2
    # ultra96

    create_project pynq_fir ./pynq_fir -part xczu3eg-sbva484-1-e
    set_property board_part em.avnet.com:ultra96:part0:1.2 [current_project]

    # create block design
    create_bd_design "fir1"
    update_compile_order -fileset sources_1

    # add zynq
    create_bd_cell -type ip -vlnv xilinx.com:ip:zynq_ultra_ps_e:3.3 zynq_ultra_ps_e_0
    apply_bd_automation -rule xilinx.com:bd_rule:zynq_ultra_ps_e -config {apply_board_preset "1" }  [get_bd_cells zynq_ultra_ps_e_0]

    # add FIR
    create_bd_cell -type ip -vlnv xilinx.com:ip:fir_compiler:7.2 fir_compiler_0
    set_property -dict [list CONFIG.Data_Width.VALUE_SRC USER] [get_bd_cells fir_compiler_0]
    set_property -dict [list CONFIG.Sample_Frequency {100} CONFIG.Clock_Frequency {100} CONFIG.Data_Width {32} CONFIG.Output_Rounding_Mode {Non_Symmetric_Rounding_Up} CONFIG.Output_Width {32} CONFIG.DATA_Has_TLAST {Packet_Framing} CONFIG.M_DATA_Has_TREADY {true} CONFIG.Coefficient_Width {16} CONFIG.Coefficient_Structure {Inferred} CONFIG.Filter_Architecture {Systolic_Multiply_Accumulate} CONFIG.ColumnConfig {11}] [get_bd_cells fir_compiler_0]
    set_property -dict [list CONFIG.CoefficientVector {-255,-260,-312,-288,-144,153,616,1233,1963,2739,3474,4081,4481,4620,4481,4081,3474,2739,1963,1233,616,153,-144,-288,-312,-260,-255} CONFIG.Coefficient_Sets {1} CONFIG.Coefficient_Sign {Signed} CONFIG.Quantization {Integer_Coefficients} CONFIG.Coefficient_Width {16} CONFIG.Coefficient_Fractional_Bits {0} CONFIG.Coefficient_Structure {Inferred} CONFIG.Data_Width {32} CONFIG.Output_Width {32} CONFIG.ColumnConfig {14}] [get_bd_cells fir_compiler_0]



    # AXI DMA
    create_bd_cell -type ip -vlnv xilinx.com:ip:axi_dma:7.1 axi_dma_0
    set_property -dict [list CONFIG.c_include_sg {0} CONFIG.c_sg_length_width {23} CONFIG.c_sg_include_stscntrl_strm {0}] [get_bd_cells axi_dma_0]


    # connection
    connect_bd_intf_net [get_bd_intf_pins fir_compiler_0/M_AXIS_DATA] [get_bd_intf_pins axi_dma_0/S_AXIS_S2MM]
    connect_bd_intf_net [get_bd_intf_pins axi_dma_0/M_AXIS_MM2S] [get_bd_intf_pins fir_compiler_0/S_AXIS_DATA]


    # add HP Slave
    set_property -dict [list CONFIG.PSU__USE__S_AXI_GP2 {1}] [get_bd_cells zynq_ultra_ps_e_0]

    # connection automation
    apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config { Clk_master {Auto} Clk_slave {Auto} Clk_xbar {Auto} Master {/axi_dma_0/M_AXI_MM2S} Slave {/zynq_ultra_ps_e_0/S_AXI_HP0_FPD} intc_ip {Auto} master_apm {0}}  [get_bd_intf_pins zynq_ultra_ps_e_0/S_AXI_HP0_FPD]
    apply_bd_automation -rule xilinx.com:bd_rule:clkrst -config {Clk "/zynq_ultra_ps_e_0/pl_clk0 (100 MHz)" }  [get_bd_pins fir_compiler_0/aclk]
    apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config { Clk_master {Auto} Clk_slave {Auto} Clk_xbar {Auto} Master {/zynq_ultra_ps_e_0/M_AXI_HPM0_FPD} Slave {/axi_dma_0/S_AXI_LITE} intc_ip {New AXI Interconnect} master_apm {0}}  [get_bd_intf_pins axi_dma_0/S_AXI_LITE]

    apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config { Clk_master {Auto} Clk_slave {/zynq_ultra_ps_e_0/pl_clk0 (100 MHz)} Clk_xbar {/zynq_ultra_ps_e_0/pl_clk0 (100 MHz)} Master {/zynq_ultra_ps_e_0/M_AXI_HPM1_FPD} Slave {/axi_dma_0/S_AXI_LITE} intc_ip {/ps8_0_axi_periph} master_apm {0}}  [get_bd_intf_pins zynq_ultra_ps_e_0/M_AXI_HPM1_FPD]

    apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config { Clk_master {/zynq_ultra_ps_e_0/pl_clk0 (100 MHz)} Clk_slave {/zynq_ultra_ps_e_0/pl_clk0 (100 MHz)} Clk_xbar {/zynq_ultra_ps_e_0/pl_clk0 (100 MHz)} Master {/axi_dma_0/M_AXI_S2MM} Slave {/zynq_ultra_ps_e_0/S_AXI_HP0_FPD} intc_ip {/axi_smc} master_apm {0}}  [get_bd_intf_pins axi_dma_0/M_AXI_S2MM]

    # rename
    set_property name fir_dma [get_bd_cells axi_dma_0]
    set_property name fir [get_bd_cells fir_compiler_0]
    group_bd_cells filter [get_bd_cells fir] [get_bd_cells fir_dma]



    regenerate_bd_layout
    validate_bd_design
    save_bd_design


    # create wrapper
    make_wrapper -files [get_files ./pynq_fir/pynq_fir.srcs/sources_1/bd/fir1/fir1.bd] -top
    add_files -norecurse ./pynq_fir/pynq_fir.srcs/sources_1/bd/fir1/hdl/fir1_wrapper.v


    launch_runs impl_1 -to_step write_bitstream -jobs 40



.. _`fig_zynq_fir3`:

.. figure:: img/zynq_fir/zynq_fir3.jpg
    :width: 100%

    FIR and DMA


















Blinking LED and FIR
====================

Vivado
------

* Run the below tcl script in Vivado 2019.2 to generate the design which is shown in :numref:`fig_zynq_fir1`

.. code-block:: tcl

    # Vivado 2019.2 
    # ultra96

    create_project zynq_fir ./zynq_fir -part xczu3eg-sbva484-1-e
    set_property board_part em.avnet.com:ultra96:part0:1.2 [current_project]

    # create block design
    create_bd_design "fir1"
    update_compile_order -fileset sources_1

    # Zynq
    create_bd_cell -type ip -vlnv xilinx.com:ip:zynq_ultra_ps_e:3.3 zynq_ultra_ps_e_0
    apply_bd_automation -rule xilinx.com:bd_rule:zynq_ultra_ps_e -config {apply_board_preset "1" }  [get_bd_cells zynq_ultra_ps_e_0]

    # connect clock
    connect_bd_net [get_bd_pins zynq_ultra_ps_e_0/pl_clk0] [get_bd_pins zynq_ultra_ps_e_0/maxihpm0_fpd_aclk]
    connect_bd_net [get_bd_pins zynq_ultra_ps_e_0/maxihpm1_fpd_aclk] [get_bd_pins zynq_ultra_ps_e_0/pl_clk0]

    # add FIR
    create_bd_cell -type ip -vlnv xilinx.com:ip:fir_compiler:7.2 fir_compiler_0

    # connect FIR to zynq and run connection automation 
    apply_bd_automation -rule xilinx.com:bd_rule:axi4_s2mm -config {Dest_Intf "/fir_compiler_0/S_AXIS_DATA" Bridge_IP "New AXI-Stream FIFO (Medium/Low frequency transfer)" Conn_M_AXIS_DATA "1" Clk_Stream "Auto" Clk_MM "/zynq_ultra_ps_e_0/pl_clk0 (100 MHz)" }  [get_bd_intf_pins zynq_ultra_ps_e_0/M_AXI_HPM0_FPD]

    apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config { Clk_master {/zynq_ultra_ps_e_0/pl_clk0 (100 MHz)} Clk_slave {/zynq_ultra_ps_e_0/pl_clk0 (100 MHz)} Clk_xbar {/zynq_ultra_ps_e_0/pl_clk0 (100 MHz)} Master {/zynq_ultra_ps_e_0/M_AXI_HPM1_FPD} Slave {/axi_fifo_mm_s/S_AXI} ddr_seg {Auto} intc_ip {/ps8_0_axi_periph} master_apm {0}}  [get_bd_intf_pins zynq_ultra_ps_e_0/M_AXI_HPM1_FPD]


    validate_bd_design
    regenerate_bd_layout
    save_bd_design


    make_wrapper -files [get_files ./zynq_fir/zynq_fir.srcs/sources_1/bd/fir1/fir1.bd] -top
    add_files -norecurse ./zynq_fir/zynq_fir.srcs/sources_1/bd/fir1/hdl/fir1_wrapper.v


    launch_runs impl_1 -to_step write_bitstream -jobs 56



* Export the design with bitstream. 


.. _`fig_zynq_fir1`:

.. figure:: img/zynq_fir/zynq_fir1.jpg
    :width: 100%

    Create the design 


Blinking LED application
------------------------

* Create a new application project (i.e. hello world with c application) in vitis with name 'zynq_fir_test' with the .xsa file which is genreated in above step. 
  

* Modify the content of the c file as below, 

.. code-block:: c

    // Blinking LED

    #include <stdio.h>
    #include "platform.h"
    #include "xil_printf.h"


    // // ************** Hello world ***************
    //int main()
    //{
    //    init_platform();
    //
    //    print("Hello World\n\r");
    //
    //    cleanup_platform();
    //    return 0;
    //}


    // // ************** Blinking LED ***************

    #include "xgpiops.h"


    #define mio_led0 20
    #define mio_led1 19
    #define mio_led2 18
    #define mio_led3 17

    /*
     * The following are declared globally so they are zeroed and can be
     * easily accessible from a debugger.
     */
    /**
     * The XGpioPs driver instance data. The user is required to allocate a
     * variable of this type for the GPIO device in the system. A pointer
     * to a variable of this type is then passed to the driver API functions.
     */
    XGpioPs Gpio;   /* The driver instance for GPIO Device. */


    int main()
    {

        init_platform();  // initialize UART

        printf("MIO LED Blinking \n\r");
        int Status;

        // GPIO Driver
        XGpioPs_Config *GPIOConfigPtr; // Create pointer for store the entry of the GPIO driver

        // look for the device configuration based on the unique device ID.
        // Return the pointer to table entry for the device IP
        GPIOConfigPtr = XGpioPs_LookupConfig(XPAR_XGPIOPS_0_DEVICE_ID);

        // initializes a XGpioPs instance/driver
        // * This function initializes a XGpioPs instance/driver.
        // * All members of the XGpioPs instance structure are initialized and
        // * StubHandlers are assigned to the Bank Status Handlers.
        Status = XGpioPs_CfgInitialize(&Gpio, GPIOConfigPtr, GPIOConfigPtr->BaseAddr);

        // check status of initialization
        if (Status != XST_SUCCESS) {
            return XST_FAILURE;
        }


        XGpioPs_SetDirectionPin(&Gpio, mio_led0, 1); // 1 : output, 0: input
        XGpioPs_SetOutputEnablePin(&Gpio, mio_led0, 1); // 1 : enable output, 0: disable output
        XGpioPs_SetDirectionPin(&Gpio, mio_led1, 1);
        XGpioPs_SetOutputEnablePin(&Gpio, mio_led1, 1);
        XGpioPs_SetDirectionPin(&Gpio, mio_led2, 1);
        XGpioPs_SetOutputEnablePin(&Gpio, mio_led2, 0); // Disable LED2 (initialized value as 1)
        XGpioPs_SetDirectionPin(&Gpio, mio_led3, 1);
        XGpioPs_SetOutputEnablePin(&Gpio, mio_led3, 1);
        while(1){
                XGpioPs_WritePin(&Gpio, mio_led0, 0x1); // write 1 to LED0
                XGpioPs_WritePin(&Gpio, mio_led1, 0x0);
                XGpioPs_WritePin(&Gpio, mio_led2, 0x0); // no effect as LED2 is disabled
                XGpioPs_WritePin(&Gpio, mio_led3, 0x0);
                usleep(500000);                         // wait for microsecond
                XGpioPs_WritePin(&Gpio, mio_led0, 0x0);
                XGpioPs_WritePin(&Gpio, mio_led1, 0x1);
                XGpioPs_WritePin(&Gpio, mio_led2, 0x0);
                XGpioPs_WritePin(&Gpio, mio_led3, 0x0);
                usleep(500000);
                XGpioPs_WritePin(&Gpio, mio_led0, 0x0);
                XGpioPs_WritePin(&Gpio, mio_led1, 0x0);
                XGpioPs_WritePin(&Gpio, mio_led2, 0x0);
                XGpioPs_WritePin(&Gpio, mio_led3, 0x1);
                usleep(500000);
                XGpioPs_WritePin(&Gpio, mio_led0, 0x0);
                XGpioPs_WritePin(&Gpio, mio_led1, 0x0);
                XGpioPs_WritePin(&Gpio, mio_led2, 0x1);
                XGpioPs_WritePin(&Gpio, mio_led3, 0x0);
                usleep(500000);
         }

        cleanup_platform();
        return 0;
    }




* Modify the UART settings in BSP. 
* Build the project and copy the BOOT.bin in SD card and test on FPGA. 
  












New Code
========


FIFO
----


.. code-block:: c

    #include <stdio.h>
    #include "xaxidma.h"
    #include "xparameters.h"
    #include "xil_printf.h"


    /******************** Constant Definitions **********************************/

    // Device hardware build related constants.
    #define DMA_DEV_ID      XPAR_AXIDMA_0_DEVICE_ID

    #define MEM_BASE_ADDR       0x01000000
    #define TX_BUFFER_BASE      (MEM_BASE_ADDR + 0x00100000)
    #define RX_BUFFER_BASE      (MEM_BASE_ADDR + 0x00300000)
    #define RX_BUFFER_HIGH      (MEM_BASE_ADDR + 0x004FFFFF)

    #define NUM_TABLE    0x5  // number to print the table
    #define NUM_ITER     0xa // print table upto NUM_ITER



    // Device instance definitions
    XAxiDma AxiDma;


    int main()
    {
        int Status;

        xil_printf("\r\n--- Entering main() --- \r\n");

        XAxiDma_Config *CfgPtr;
        int Index;
        u8 *TxBufferPtr;
        u8 *RxBufferPtr;
        u8 Value;

        TxBufferPtr = (u8 *)TX_BUFFER_BASE ;
        RxBufferPtr = (u8 *)RX_BUFFER_BASE;

        /* Initialize the XAxiDma device.
         */
        CfgPtr = XAxiDma_LookupConfig(DMA_DEV_ID);
        if (!CfgPtr) {
            xil_printf("No config found for %d\r\n", DMA_DEV_ID);
            return XST_FAILURE;
        }

        Status = XAxiDma_CfgInitialize(&AxiDma, CfgPtr);
        if (Status != XST_SUCCESS) {
            xil_printf("Initialization failed %d\r\n", Status);
            return XST_FAILURE;
        }

        if(XAxiDma_HasSg(&AxiDma)){
            xil_printf("Device configured as SG mode \r\n");
            return XST_FAILURE;
        }

        /* Disable interrupts, we use polling mode
         */
        XAxiDma_IntrDisable(&AxiDma, XAXIDMA_IRQ_ALL_MASK,
                            XAXIDMA_DEVICE_TO_DMA);
        XAxiDma_IntrDisable(&AxiDma, XAXIDMA_IRQ_ALL_MASK,
                            XAXIDMA_DMA_TO_DEVICE);



        printf("################# Tx data ######################\n");
        for(Index = 0; Index < NUM_ITER; Index ++) {
                Value = NUM_TABLE*Index;
                TxBufferPtr[Index] = Value;
                printf("Value[%d]: %d\n", Index, Value);

        }



        // flush the TX buffer
        Xil_DCacheFlushRange((UINTPTR)TxBufferPtr, NUM_ITER);

        printf(" Before Tx -> 4h: %x, 34h: %x\n\n", XAxiDma_ReadReg(XPAR_AXI_DMA_0_BASEADDR, 0x04), XAxiDma_ReadReg(XPAR_AXI_DMA_0_BASEADDR, 0x34));

        Status = XAxiDma_SimpleTransfer(&AxiDma,(UINTPTR) TxBufferPtr,
                    NUM_ITER, XAXIDMA_DMA_TO_DEVICE);

        if (Status != XST_SUCCESS) {
            return XST_FAILURE;
        }

        printf(" After Tx -> 4h: %x, 34h: %x\n\n", XAxiDma_ReadReg(XPAR_AXI_DMA_0_BASEADDR, 0x04), XAxiDma_ReadReg(XPAR_AXI_DMA_0_BASEADDR, 0x34));

        printf("Waiting for completion of XAXIDMA_DMA_TO_DEVICE\n");
       while (XAxiDma_Busy(&AxiDma,XAXIDMA_DMA_TO_DEVICE)){}
        printf("Done\n");

        printf(" After XAxiDma_Busy-XAXIDMA_DMA_TO_DEVICE -> 4h: %x, 34h: %x\n\n", XAxiDma_ReadReg(XPAR_AXI_DMA_0_BASEADDR, 0x04), XAxiDma_ReadReg(XPAR_AXI_DMA_0_BASEADDR, 0x34));









        // while ((XAxiDma_Busy(&AxiDma,XAXIDMA_DEVICE_TO_DMA)) ||
        //            (XAxiDma_Busy(&AxiDma,XAXIDMA_DMA_TO_DEVICE))) {
        //        }



        // flush the RX buffer
        Xil_DCacheFlushRange((UINTPTR)RxBufferPtr, NUM_ITER);

        printf(" Before Rx -> 4h: %x, 34h: %x\n\n", XAxiDma_ReadReg(XPAR_AXI_DMA_0_BASEADDR, 0x04), XAxiDma_ReadReg(XPAR_AXI_DMA_0_BASEADDR, 0x34));

        Status = XAxiDma_SimpleTransfer(&AxiDma,(UINTPTR) RxBufferPtr,
                            NUM_ITER, XAXIDMA_DEVICE_TO_DMA);

        if (Status != XST_SUCCESS) {
            return XST_FAILURE;
        }

        printf(" After Rx -> 4h: %x, 34h: %x\n\n", XAxiDma_ReadReg(XPAR_AXI_DMA_0_BASEADDR, 0x04), XAxiDma_ReadReg(XPAR_AXI_DMA_0_BASEADDR, 0x34));

        printf("Waiting for completion of XAXIDMA_DEVICE_TO_DMA\n");
       while (XAxiDma_Busy(&AxiDma,XAXIDMA_DEVICE_TO_DMA)){}
        printf("Done\n");


        printf(" After XAxiDma_Busy-XAXIDMA_DEVICE_TO_DMA -> 4h: %x, 34h: %x\n\n", XAxiDma_ReadReg(XPAR_AXI_DMA_0_BASEADDR, 0x04), XAxiDma_ReadReg(XPAR_AXI_DMA_0_BASEADDR, 0x34));


        printf("\n################# Rx data ######################\n");
        for(Index = 0; Index < NUM_ITER; Index ++) {
                printf("Value[%d]: %d\n", Index, RxBufferPtr[Index]);
        }


        return XST_SUCCESS;

    }





Result 

.. code-block:: text

    --- Entering main() ---
    ################# Tx data ######################
    Value[0]: 0
    Value[1]: 5
    Value[2]: 10
    Value[3]: 15
    Value[4]: 20
    Value[5]: 25
    Value[6]: 30
    Value[7]: 35
    Value[8]: 40
    Value[9]: 45
     Before Tx -> 4h: 1, 34h: 1

     After Tx -> 4h: 0, 34h: 1

    Waiting for completion of XAXIDMA_DMA_TO_DEVICE
    Done
     After XAxiDma_Busy-XAXIDMA_DMA_TO_DEVICE -> 4h: 1002, 34h: 1

     Before Rx -> 4h: 1002, 34h: 1

     After Rx -> 4h: 1002, 34h: 0

    Waiting for completion of XAXIDMA_DEVICE_TO_DMA
    Done
     After XAxiDma_Busy-XAXIDMA_DEVICE_TO_DMA -> 4h: 1002, 34h: 1002


    ################# Rx data ######################
    Value[0]: 0
    Value[1]: 5
    Value[2]: 10
    Value[3]: 15
    Value[4]: 20
    Value[5]: 25
    Value[6]: 30
    Value[7]: 35
    Value[8]: 40
    Value[9]: 45



FIR
---



.. code-block:: c

    #include <stdio.h>
    #include "xaxidma.h"
    #include "xparameters.h"
    #include "xil_printf.h"


    /******************** Constant Definitions **********************************/

    // Device hardware build related constants.
    #define DMA_DEV_ID      XPAR_FILTER_FIR_DMA_DEVICE_ID

    #define MEM_BASE_ADDR       0x01000000
    #define TX_BUFFER_BASE      (MEM_BASE_ADDR + 0x00100000)
    #define RX_BUFFER_BASE      (MEM_BASE_ADDR + 0x00300000)
    #define RX_BUFFER_HIGH      (MEM_BASE_ADDR + 0x004FFFFF)

    #define NUM_TABLE    0x5  // number to print the table
    #define NUM_ITER     0xa // print table upto NUM_ITER



    // Device instance definitions
    XAxiDma AxiDma;


    int main()
    {
        int Status;

        xil_printf("\r\n--- Entering main() --- \r\n");

        XAxiDma_Config *CfgPtr;
        int Index;
        u8 *TxBufferPtr;
        u8 *RxBufferPtr;
        u8 Value;

        TxBufferPtr = (u8 *)TX_BUFFER_BASE ;
        RxBufferPtr = (u8 *)RX_BUFFER_BASE;

        /* Initialize the XAxiDma device.
         */
        CfgPtr = XAxiDma_LookupConfig(DMA_DEV_ID);
        if (!CfgPtr) {
            xil_printf("No config found for %d\r\n", DMA_DEV_ID);
            return XST_FAILURE;
        }

        Status = XAxiDma_CfgInitialize(&AxiDma, CfgPtr);
        if (Status != XST_SUCCESS) {
            xil_printf("Initialization failed %d\r\n", Status);
            return XST_FAILURE;
        }

        if(XAxiDma_HasSg(&AxiDma)){
            xil_printf("Device configured as SG mode \r\n");
            return XST_FAILURE;
        }

        /* Disable interrupts, we use polling mode
         */
        XAxiDma_IntrDisable(&AxiDma, XAXIDMA_IRQ_ALL_MASK,
                            XAXIDMA_DEVICE_TO_DMA);
        XAxiDma_IntrDisable(&AxiDma, XAXIDMA_IRQ_ALL_MASK,
                            XAXIDMA_DMA_TO_DEVICE);



        printf("################# Tx data ######################\n");
        for(Index = 0; Index < NUM_ITER; Index ++) {
                Value = NUM_TABLE*Index;
                TxBufferPtr[Index] = Value;
                printf("Value[%d]: %d\n", Index, Value);

        }



        // flush the TX buffer
        Xil_DCacheFlushRange((UINTPTR)TxBufferPtr, NUM_ITER);

        printf(" Before Tx -> 4h: %x, 34h: %x\n\n", XAxiDma_ReadReg( XPAR_FILTER_FIR_DMA_BASEADDR, 0x04), XAxiDma_ReadReg( XPAR_FILTER_FIR_DMA_BASEADDR, 0x34));

        Status = XAxiDma_SimpleTransfer(&AxiDma,(UINTPTR) TxBufferPtr,
                    NUM_ITER, XAXIDMA_DMA_TO_DEVICE);

        if (Status != XST_SUCCESS) {
            return XST_FAILURE;
        }

        printf(" After Tx -> 4h: %x, 34h: %x\n\n", XAxiDma_ReadReg( XPAR_FILTER_FIR_DMA_BASEADDR, 0x04), XAxiDma_ReadReg( XPAR_FILTER_FIR_DMA_BASEADDR, 0x34));

        printf("Waiting for completion of XAXIDMA_DMA_TO_DEVICE\n");
       while (XAxiDma_Busy(&AxiDma,XAXIDMA_DMA_TO_DEVICE)){}
        printf("Done\n");

        printf(" After XAxiDma_Busy-XAXIDMA_DMA_TO_DEVICE -> 4h: %x, 34h: %x\n\n", XAxiDma_ReadReg( XPAR_FILTER_FIR_DMA_BASEADDR, 0x04), XAxiDma_ReadReg( XPAR_FILTER_FIR_DMA_BASEADDR, 0x34));









        // while ((XAxiDma_Busy(&AxiDma,XAXIDMA_DEVICE_TO_DMA)) ||
        //            (XAxiDma_Busy(&AxiDma,XAXIDMA_DMA_TO_DEVICE))) {
        //        }



        // flush the RX buffer
        Xil_DCacheFlushRange((UINTPTR)RxBufferPtr, NUM_ITER);

        printf(" Before Rx -> 4h: %x, 34h: %x\n\n", XAxiDma_ReadReg( XPAR_FILTER_FIR_DMA_BASEADDR, 0x04), XAxiDma_ReadReg( XPAR_FILTER_FIR_DMA_BASEADDR, 0x34));

        Status = XAxiDma_SimpleTransfer(&AxiDma,(UINTPTR) RxBufferPtr,
                            NUM_ITER, XAXIDMA_DEVICE_TO_DMA);

        if (Status != XST_SUCCESS) {
            return XST_FAILURE;
        }

        printf(" After Rx -> 4h: %x, 34h: %x\n\n", XAxiDma_ReadReg( XPAR_FILTER_FIR_DMA_BASEADDR, 0x04), XAxiDma_ReadReg( XPAR_FILTER_FIR_DMA_BASEADDR, 0x34));

        printf("Waiting for completion of XAXIDMA_DEVICE_TO_DMA\n");
       while (XAxiDma_Busy(&AxiDma,XAXIDMA_DEVICE_TO_DMA)){}
        printf("Done\n");


        printf(" After XAxiDma_Busy-XAXIDMA_DEVICE_TO_DMA -> 4h: %x, 34h: %x\n\n", XAxiDma_ReadReg( XPAR_FILTER_FIR_DMA_BASEADDR, 0x04), XAxiDma_ReadReg( XPAR_FILTER_FIR_DMA_BASEADDR, 0x34));


        printf("\n################# Rx data ######################\n");
        for(Index = 0; Index < NUM_ITER; Index ++) {
                printf("Value[%d]: %d\n", Index, RxBufferPtr[Index]);
        }


        return XST_SUCCESS;

    }





Result 

.. code-block:: text

    --- Entering main() ---
    ################# Tx data ######################
    Value[0]: 0
    Value[1]: 5
    Value[2]: 10
    Value[3]: 15
    Value[4]: 20
    Value[5]: 25
    Value[6]: 30
    Value[7]: 35
    Value[8]: 40
    Value[9]: 45
     Before Tx -> 4h: 1, 34h: 1

     After Tx -> 4h: 0, 34h: 1

    Waiting for completion of XAXIDMA_DMA_TO_DEVICE
    Done
     After XAxiDma_Busy-XAXIDMA_DMA_TO_DEVICE -> 4h: 1002, 34h: 1

     Before Rx -> 4h: 1002, 34h: 1

     After Rx -> 4h: 1002, 34h: 0

    Waiting for completion of XAXIDMA_DEVICE_TO_DMA
