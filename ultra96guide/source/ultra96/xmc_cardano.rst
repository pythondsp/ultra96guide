Cardano with XMC
****************


Complex Number addition : single port
=====================================


Create Kernel
-------------

First create the kernels for the adder (i.e. .h and .cpp file) which defines the functionality of the adder. 
In the below code, we have 

    * one input port 'a' and one output port 's'. 
    * input and output are complex number. 
    * constant values are added to complex number i.e. 3 to real part and 5 to imaginary part. 


.. code-block:: cpp

    // kernels.h

    #include <cardano.h>
    #ifndef FUNCTION_KERNELS_H
    #define FUNCTION_KERNELS_H
     
    #define NUM_SAMPLES 1
    #define WINDOW_SIZE 1

      void add_num(input_window_cint16 * a, output_window_cint16 * s);
     
    #endif



.. code-block:: cpp

    // kernels.cpp
     
    #include <kernels.h> 
     
    void add_num(input_window_cint16 * a, output_window_cint16 * s) {
      cint16 c1, c3;
      for (unsigned i=0; i<NUM_SAMPLES; i++) {  // print first 40 outputs
        window_readincr(a, c1);
        c3.real = c1.real + 3;  // add 3 to input value
        c3.imag = c1.imag + 5;
        window_writeincr(s, c3);
      }
    }



Add kernel to XMC
-----------------


Open model composer and add 'AIE kernel' to simulink model. For this 

* go to matlab and type, 

.. code-block:: matlab

    >>> open ModelComposer.slx


* Look for the "AIE Kernel" and add it to simulink model as shown in :numref:`fig_cardano_xmc1`. 
  
  .. _`fig_cardano_xmc1`:

.. figure:: img/cardano_xmc/cardano_xmc1.jpg
    :width: 60%

    Add AIE Kernel to simulink model


* Double click on AIE Kernel block and add the 'kernels.h', 'kernels.cpp' and 'function name' as shown in :numref:`fig_cardano_xmc2`. Click on 'apply' and then 'import'
  

.. _`fig_cardano_xmc2`:

.. figure:: img/cardano_xmc/cardano_xmc2.jpg
    :width: 100%

    Add kernel to XMC block


* Now, we can see the input and output port to AIE Kernel block. Double click the block again and enter the 'window size' and 'signal size' as shown in :numref:`fig_cardano_xmc3`. These are used for burst data. Currently, we are using only one sample at at time. 
  


.. _`fig_cardano_xmc3`:

.. figure:: img/cardano_xmc/cardano_xmc3.jpg
    :width: 100%

    Define input and output size


Complete design and run simulation
----------------------------------

* Complete the design as shown in :numref:`fig_cardano_xmc4` by adding a constant and display block. 
  

.. _`fig_cardano_xmc4`:

.. figure:: img/cardano_xmc/cardano_xmc4.jpg
    :width: 100%

    Complete the design


* Run the simulation to see the output as shown in :numref:`fig_cardano_xmc5`
  

.. _`fig_cardano_xmc5`:

.. figure:: img/cardano_xmc/cardano_xmc5.jpg
    :width: 100%

    Run simulation : output = Real + 3, Imag + 5


Real number addition : two ports
================================


* In the below code, we are using real number (i.e. cint16 is changed to int16); and the design has two input ports i.e. a and b. Output s is the sum of a and b. 


.. error:: Seeing issue with int16 therefore int32 is used for input/output type.




.. code-block:: cpp

    // kernels.h

    #include <cardano.h>
    #ifndef FUNCTION_KERNELS_H
    #define FUNCTION_KERNELS_H
     
    #define NUM_SAMPLES 1
    #define WINDOW_SIZE 1

      void add_num(input_window_int32 * a, input_window_int32 * b, output_window_int32 * s);
     
    #endif


.. code-block:: cpp

    // kernels.cpp
     
    #include <kernels.h> 
     
    void add_num(input_window_int32 * a, input_window_int32 * b, output_window_int32 * s) {
      int32 c1, c2, c3;
      for (unsigned i=0; i<NUM_SAMPLES; i++) {  // print first 40 outputs
        window_readincr(a, c1);
        window_readincr(b, c2);
        c3 = c1 + c2;  // add c1 and c2
        window_writeincr(s, c3);
      }
    }

* Double click on AIE Kernel; and click on update button as shown in :numref:`fig_cardano_xmc6`
  
.. _`fig_cardano_xmc6`:

.. figure:: img/cardano_xmc/cardano_xmc6.jpg
    :width: 100%

    Update the kernel
  
* Verify the parameters as shown in :numref:`fig_cardano_xmc8`

.. _`fig_cardano_xmc8`:

.. figure:: img/cardano_xmc/cardano_xmc8.jpg
    :width: 100%

    Set the parameters correctly 

* Complete the design as shown in :numref:`fig_cardano_xmc7`. 
  
.. _`fig_cardano_xmc7`:

.. figure:: img/cardano_xmc/cardano_xmc7.jpg
    :width: 100%

    Modify the input type and values


* Run the simulation to see the results in :numref:`fig_cardano_xmc9`

.. _`fig_cardano_xmc9`:

.. figure:: img/cardano_xmc/cardano_xmc9.jpg
    :width: 100%

    simulation result



Multiple output port
====================


* In previous section, we have added two input ports. In this section, we will use two output port. And one port will display the sum of two numbers and other will show the difference between two numbers. 


* Modify the code as below, 

.. code-block:: cpp

    // kernels.h

    #include <cardano.h>
    #ifndef FUNCTION_KERNELS_H
    #define FUNCTION_KERNELS_H
     
    #define NUM_SAMPLES 1
    #define WINDOW_SIZE 1

      void add_num(input_window_int32 * a, input_window_int32 * b, output_window_int32 * sum, output_window_int32 * diff);
     
    #endif


.. code-block:: cpp

    // kernels.cpp
     
    #include <kernels.h> 
     
    void add_num(input_window_int32 * a, input_window_int32 * b, output_window_int32 * sum, output_window_int32 * diff) {
      int32 c1, c2, c3, c4;
      for (unsigned i=0; i<NUM_SAMPLES; i++) {  // print first 40 outputs
        window_readincr(a, c1);
        window_readincr(b, c2);
        c3 = c1 + c2;  // add c1 and c2
        c4 = c1 - c2;
        window_writeincr(sum, c3);
        window_writeincr(diff, c4);
      }
    }



* Update the AIE Kernel. 
* Modify the design and run the simulation as shown in :numref:`fig_cardano_xmc10`

.. _`fig_cardano_xmc10`:

.. figure:: img/cardano_xmc/cardano_xmc10.jpg
    :width: 100%

    simulation result