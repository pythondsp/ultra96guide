Hello World and Blinking LED
****************************


Introduction
============


In this chapter, we will create an "Hello World" application for Ultra96-V1 board.

.. _`vivado_project_ultra96`:

Create Vivado project
=====================

* First instatiate and make connection for Zynq processor using below tcl script, 

.. code-block:: tcl

    # hello_world.tcl

    # ULTRA96 V1 board
    # vivado 2018.2

    # create project
    create_project hello_world ./hello_world -part xczu3eg-sbva484-1-e -force
    set_property board_part em.avnet.com:ultra96:part0:1.0 [current_project]

    #create block design
    create_bd_design "test1"
    update_compile_order -fileset sources_1

    # zynq processor
    create_bd_cell -type ip -vlnv xilinx.com:ip:zynq_ultra_ps_e:3.2 zynq_ultra_ps_e_0
    apply_bd_automation -rule xilinx.com:bd_rule:zynq_ultra_ps_e -config {apply_board_preset "1" }  [get_bd_cells zynq_ultra_ps_e_0]


    connect_bd_net [get_bd_pins zynq_ultra_ps_e_0/pl_clk0] [get_bd_pins zynq_ultra_ps_e_0/maxihpm0_fpd_aclk]
    connect_bd_net [get_bd_pins zynq_ultra_ps_e_0/pl_clk0] [get_bd_pins zynq_ultra_ps_e_0/maxihpm1_fpd_aclk]


    # validate and save design
    validate_bd_design
    regenerate_bd_layout
    save_bd_design


    # create wrapper
    make_wrapper -files [get_files ./hello_world/hello_world.srcs/sources_1/bd/test1/test1.bd] -top
    add_files -norecurse ./hello_world/hello_world.srcs/sources_1/bd/test1/hdl/test1_wrapper.v

    # generate bit stream
    launch_runs impl_1 -to_step write_bitstream -jobs 40


* :numref:`fig_helloworld1` will be implemented by above script, 

.. _`fig_helloworld1`:

.. figure:: img/helloworld/helloworld1.jpg
    :width: 75%

    Zynq processor connection


* Next extport the hardware i.e. go to "File -> Export -> Export Hardware". Next, select "include bitstream" and "local to project". 

* Then lauch SDK i.e. "File -> Launch SDK". 
  

.. _`hello_world_jtag`:

Hello World using JTAG
======================


* In the SDK window, create "Hello world application" i.e. go to "File -> New -> Application Project". 
* Create project with settings which are shown in :numref:`fig_helloworld2` and :numref:`fig_helloworld3`, 

.. _`fig_helloworld2`:

.. figure:: img/helloworld/helloworld2.jpg
    :width: 75%

    Create application


.. _`fig_helloworld3`:

.. figure:: img/helloworld/helloworld3.jpg
    :width: 75%

    Select hello world application


* Below is the content of "helloworld.c" file, 

.. code-block:: c
    :caption: C code for Hello world 
    :name: c_hellow_world

    #include <stdio.h>
    #include "platform.h"
    #include "xil_printf.h"


    int main()
    {
        init_platform();

        print("Hello World\n\r");

        cleanup_platform();
        return 0;
    }


* Next, modify the 'UART settings' in the bsp file. First, open the modify bsp option as shown in :numref:`fig_helloworld4`
  
.. _`fig_helloworld4`:

.. figure:: img/helloworld/helloworld4.jpg
    :width: 75%

    Modify BSP

* Then, select 'psu_uart1' for 'stdin' and 'stdout' as shown in :numref:`fig_helloworld5`
  
.. _`fig_helloworld5`:

.. figure:: img/helloworld/helloworld5.jpg
    :width: 75%

    Select psu_uart1 for stdin and stdout


* Now, build the hello world application as shown in :numref:`fig_helloworld6`
  
.. _`fig_helloworld6`:

.. figure:: img/helloworld/helloworld6.jpg
    :width: 75%

    Build application


Run the SDK application using JTAG
==================================

Application can be run using "JTAG" and "SD card". JTAG mode is used in this section. 


Note that, we will need the "AVNET's USB to JTAG/UART connector" to use this section. Please see :numref:`sec_sdcard_mode` in the absence of this connector where SD card is use to run this application.

.. _`fig_helloworld7`:

.. figure:: img/helloworld/helloworld7.jpg
    :width: 75%

    AVNET's USB to JTAG/UART connector


* First, put the FPGA in JTAG mode and then connect the JTAG connector to FPGA; and connect it to your computer. Turn on the FPGA and program it by click the 'program fpga' button (with default settings) as shown in :numref:`fig_helloworld8` 

.. _`fig_helloworld8`:

.. figure:: img/helloworld/helloworld8.jpg
    :width: 75%

    Program FPGA

* Now, select the correct the serial port in "SDK terminal" as shown in :numref:`fig_helloworld9`. In this termial, we will see the 'hello world message'

.. _`fig_helloworld9`:

.. figure:: img/helloworld/helloworld9.jpg
    :width: 75%

    Select 'serial port' to see the message

* Now, launch the hello world application on FPGA as shown in :numref:`fig_helloworld10`


.. _`fig_helloworld10`:

.. figure:: img/helloworld/helloworld10.jpg
    :width: 75%

    Run the 'hello world' application

* "Hello World" will be shown in SDK terminal as shown in :numref:`fig_helloworld11`

.. _`fig_helloworld11`:

.. figure:: img/helloworld/helloworld11.jpg
    :width: 75%

    "Hello World" on SDK terminal


.. _`sec_sdcard_mode`: 

Run the Hello World using SD Card
=================================

.. note:: 

    We will need the JTAG connector (see :numref:`fig_helloworld7`) to display the message using SD card. However, we will not need the JTAG to program the FPGA. We will use this method in :numref:`blinking_led_SDcard` where 'blinking LED' program will be implemented. 

* First create the "Hello world" application which is shown in :numref:`hello_world_jtag`, 
* Next, create the FSBL application as shown in :numref:`fig_helloworld12` and :numref:`fig_helloworld13`

.. _`fig_helloworld12`:

.. figure:: img/helloworld/helloworld12.jpg
    :width: 75%

    Crate FSBL project

.. _`fig_helloworld13`:

.. figure:: img/helloworld/helloworld13.jpg
    :width: 75%

    Create FSBL application 


* Next, create the Boot image as shown in :numref:`fig_helloworld14` and :numref:`fig_helloworld15` 

.. _`fig_helloworld14`:

.. figure:: img/helloworld/helloworld14.jpg
    :width: 75%

    Create Boot image


* If all the files are not shown as in :numref:`fig_helloworld15` (i.e. fsbl.elf, .bit file and hello_world.elf), then try to rebuild the application. Then manually find and add the files.  

.. _`fig_helloworld15`:

.. figure:: img/helloworld/helloworld15.jpg
    :width: 75%

    Generate image (order of file should be same i.e. fsbl.elf, .bit file and hello_world.elf)


* Now, copy the "BOOT.bin" file to SD card. 
* Next, set the FPGA to "SD card" mode and insert the SD card.
* Open teraterm and select "serial" and then select the correct com-port as shown in :numref:`fig_helloworld16`. 

.. _`fig_helloworld16`:

.. figure:: img/helloworld/helloworld16.jpg
    :width: 75%

    TeraTerm settings


* Turn on the FPGA and "Hello World" will be displayed on Teraterm as shown in :numref:`fig_helloworld17`.  

.. _`fig_helloworld17`:

.. figure:: img/helloworld/helloworld17.jpg
    :width: 75%

    Output Message


.. _`blinking_led_SDcard`:

Blinking LED using SD card
==========================


Modify the 'helloworld.c' file in :numref:`c_hellow_world` as shown in :numref:`c_blink_led`


.. code-block:: c
    :linenos: 
    :caption: Blinking LED
    :name: c_blink_led

    #include <stdio.h>
    #include "platform.h"
    #include "xil_printf.h"

    #include "xgpiops.h"


    #define mio_led0 20
    #define mio_led1 19
    #define mio_led2 18
    #define mio_led3 17

    XGpioPs Gpio;

    int main()
    {
        printf("Blinking LED test\n\r");
        int Status;
        XGpioPs_Config *GPIOConfigPtr;

        init_platform();

        GPIOConfigPtr = XGpioPs_LookupConfig(XPAR_XGPIOPS_0_DEVICE_ID);

        Status = XGpioPs_CfgInitialize(&Gpio, GPIOConfigPtr, GPIOConfigPtr ->BaseAddr);

        if (Status != XST_SUCCESS) {

        return XST_FAILURE;

        }

        XGpioPs_SetDirectionPin(&Gpio, mio_led0, 1);
        XGpioPs_SetOutputEnablePin(&Gpio, mio_led0, 1);
        XGpioPs_SetDirectionPin(&Gpio, mio_led1, 1);
        XGpioPs_SetOutputEnablePin(&Gpio, mio_led1, 1);
        XGpioPs_SetDirectionPin(&Gpio, mio_led2, 1);
        XGpioPs_SetOutputEnablePin(&Gpio, mio_led2, 1);
        XGpioPs_SetDirectionPin(&Gpio, mio_led3, 1);
        XGpioPs_SetOutputEnablePin(&Gpio, mio_led3, 1);
        while(1){
                XGpioPs_WritePin(&Gpio, mio_led0, 0x1);
                XGpioPs_WritePin(&Gpio, mio_led1, 0x0);
                XGpioPs_WritePin(&Gpio, mio_led2, 0x0);
                XGpioPs_WritePin(&Gpio, mio_led3, 0x0);
                usleep(500000);
                XGpioPs_WritePin(&Gpio, mio_led0, 0x0);
                XGpioPs_WritePin(&Gpio, mio_led1, 0x1);
                XGpioPs_WritePin(&Gpio, mio_led2, 0x0);
                XGpioPs_WritePin(&Gpio, mio_led3, 0x0);
                usleep(500000);
                XGpioPs_WritePin(&Gpio, mio_led0, 0x0);
                XGpioPs_WritePin(&Gpio, mio_led1, 0x0);
                XGpioPs_WritePin(&Gpio, mio_led2, 0x0);
                XGpioPs_WritePin(&Gpio, mio_led3, 0x1);
                usleep(500000);
                XGpioPs_WritePin(&Gpio, mio_led0, 0x0);
                XGpioPs_WritePin(&Gpio, mio_led1, 0x0);
                XGpioPs_WritePin(&Gpio, mio_led2, 0x1);
                XGpioPs_WritePin(&Gpio, mio_led3, 0x0);
                usleep(500000);
         }

        cleanup_platform();
        return 0;
    }


* Build the hello world application again. 
* Create the 'Boot image' again; and copy it to SD card.
* Boot the FPGA with SD card to see the blinking LEDs.
* Note that, the same can be run using JTAG mode as well as shown in previous sections. 

.. _`helloworldLinuxApp`:

Hello World : Linux application
===============================

In this section, we will create a petalinux image and the Hello World application for it. 

* We will use the hdf file which is generated in :numref:`vivado_project_ultra96`. 
* Run the below commands to generate the petalinux image for above hdf file, 

.. code-block:: text

    $ petalinux-create --type project --template zynqMP --name ultra96_zynqMP
    $ cd ultra96_zynqMP/

    # put the correct location of the folder which contains the hdf file
    $ petalinux-config --get-hw-description=../hello_world/hello_world.sdk/
    
    * When window pop up, then go to Subsystem AUTO Hardware Settings -> Serial Settings -> Primary Stdin/Stdout -> psu_uart_1 (save and exit)
    
    $ petalinux-build
    
    # generate BOOT.bin file
    $ petalinux-package --boot --format BIN --fsbl images/linux/zynqmp_fsbl.elf --pmufw images/linux/pmufw.elf --fpga images/linux/system.bit --u-boot images/linux/u-boot.elf --force


* Copy the BOOT.bin and image.ub file to sd card and turn on the FPGA. The linux image will boot up (username and password : root). 
  

* Next, create the Hello World application using below commands, 

.. code-block:: shell

    $ petalinux-create -t apps -n helloworld --enable

* The helloworld.c file will be created after this. Modify it as below, 

    $ vi project-spec/meta-user/recipes-apps/ocvtest/files/helloworld.c 

.. code-block:: c

    #include <stdio.h>

    int main(int argc, char **argv)
    {
        printf("Hello World!\n");
        printf("Hello World Again!\n");

        return 0;
    }

* Build the app and generate the BOOT.bin again. 
    
    $ petalinux-build -c helloworld
    $ petalinux-build
    $ petalinux-package --boot --format BIN --fsbl images/linux/zynqmp_fsbl.elf --pmufw images/linux/pmufw.elf --fpga images/linux/system.bit --u-boot images/linux/u-boot.elf --force


* Copy BOOT.bin and image.ub to SD and boot the FGPA. 
* After booting, run the hello world application as below, 

.. code-block:: shell

    root@ultra96_zynqMP:~# helloworld
    Hello World!
    Hello World Again!


Conclusion
==========

In this chapter, we saw two methods i.e. "JTAG" and "SD card" to run the "hello world" application. Both standalone and Linux application are created for it. Also, we implemented the blinking LED example in this chapter. 




.. Question : 
.. How to write blinking LED  as Linux application
.. Read sensor value to FPGA using Petalinux 
.. Read sensor value to FPGA using FPGA directly


