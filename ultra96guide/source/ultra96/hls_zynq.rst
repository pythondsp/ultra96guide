HLS Projects
************

Create HLS project
==================


* Create a new project with device "xczu3eg-sbva484-1-e"

.. code-block:: c

    // array2d_test.h

    #ifndef _ARRAY_ARITH_H_
    #define _ARRAY_ARITH_H_

    #include <stdio.h>

    typedef int di_t,do_t;

    void array2d_test (di_t d1[2][2],do_t d2[2][2]);

    #endif


.. code-block:: c

    // array2d_test.c

    #include "array2d_test.h"

    void array2d_test (di_t d1[2][2],do_t d2[2][2]) {
    #pragma HLS INTERFACE s_axilite port=d2
    #pragma HLS INTERFACE s_axilite port=d1

      int i,j;
      
        for (i=0;i<2;i++) {
            for (j=0;j<2;j++) {
                d2[i][j] = d1[i][j]+1;
            }
        }
    }


* Add 'array2d_test' as a top function in project setting; and run the 'c synthesis'. Next, click on "Export RTL" for the design. 
  

Vivado
======


.. code-block:: tcl

    ### First add the HLS IP in the project 

    create_project array2d_ultra96 /proj/css/meherp/meher/hls/srav_hls/array2d_ultra96 -part xczu3eg-sbva484-1-e
    set_property board_part em.avnet.com:ultra96:part0:1.2 [current_project]


    create_bd_design "design_1"
    update_compile_order -fileset sources_1


    # zynq 
    create_bd_cell -type ip -vlnv xilinx.com:ip:zynq_ultra_ps_e:3.3 zynq_ultra_ps_e_0
    apply_bd_automation -rule xilinx.com:bd_rule:zynq_ultra_ps_e -config {apply_board_preset "1" }  [get_bd_cells zynq_ultra_ps_e_0]


    # Add HLS IP
    create_bd_cell -type ip -vlnv xilinx.com:hls:array2d_test:1.0 array2d_test_0

    # connection
    apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config { Clk_master {Auto} Clk_slave {Auto} Clk_xbar {Auto} Master {/zynq_ultra_ps_e_0/M_AXI_HPM0_FPD} Slave {/array2d_test_0/s_axi_AXILiteS} ddr_seg {Auto} intc_ip {New AXI Interconnect} master_apm {0}}  [get_bd_intf_pins array2d_test_0/s_axi_AXILiteS]
    apply_bd_automation -rule xilinx.com:bd_rule:axi4 -config { Clk_master {Auto} Clk_slave {/zynq_ultra_ps_e_0/pl_clk0 (100 MHz)} Clk_xbar {/zynq_ultra_ps_e_0/pl_clk0 (100 MHz)} Master {/zynq_ultra_ps_e_0/M_AXI_HPM1_FPD} Slave {/array2d_test_0/s_axi_AXILiteS} ddr_seg {Auto} intc_ip {/ps8_0_axi_periph} master_apm {0}}  [get_bd_intf_pins zynq_ultra_ps_e_0/M_AXI_HPM1_FPD]

    validate_bd_design
    regenerate_bd_layout
    save_bd_design


    make_wrapper -files [get_files /proj/css/meherp/meher/hls/srav_hls/array2d_ultra96/array2d_ultra96.srcs/sources_1/bd/design_1/design_1.bd] -top
    add_files -norecurse /proj/css/meherp/meher/hls/srav_hls/array2d_ultra96/array2d_ultra96.srcs/sources_1/bd/design_1/hdl/design_1_wrapper.v



* Design in :numref:`fig_hls_zynq1` will be created with above script, 

.. _`fig_hls_zynq1`:

.. figure:: img/hls_zynq/hls_zynq1.jpg
    :width: 100%

    HLS IP with Zynq



* Add testbench: Here value for the address in write_data(32'hA0000010) and read_data(32'hA0000020) is taken from xarray2d_test_hw.h file as shwon in :numref:`fig_hls_zynq1`


.. _`fig_hls_zynq2`:

.. figure:: img/hls_zynq/hls_zynq2.jpg
    :width: 100%

    Read and write address which are used in testbench



.. code-block:: verilog

    // tb_adder2d.v

    `timescale 1ns / 1ps

    module tb_adder2d;
        reg tb_ACLK;
        reg tb_ARESETn;
       
        wire temp_clk;
        wire temp_rstn; 
      
        reg [127:0] read_data;
     
        reg resp;
        
      wire ap_done;
      wire ap_idle;
      wire ap_ready;
       reg ap_start;
      
        initial 
        begin       
            tb_ACLK = 1'b0;
        end
        
        //------------------------------------------------------------------------
        // Simple Clock Generator
        //------------------------------------------------------------------------
        
        always #10 tb_ACLK = !tb_ACLK;
           
        initial
        begin
        
            $display ("running the tb");
            
            tb_ARESETn = 1'b0;
            repeat(20)@(posedge tb_ACLK);        
            tb_ARESETn = 1'b1;
            @(posedge tb_ACLK);
            
            repeat(5) @(posedge tb_ACLK);
              
            tb_adder2d.mpsoc_sys.design_1_i.zynq_ultra_ps_e_0.inst.por_srstb_reset(1'b1); 
            #200;
            tb_adder2d.mpsoc_sys.design_1_i.zynq_ultra_ps_e_0.inst.por_srstb_reset(1'b0);
            tb_adder2d.mpsoc_sys.design_1_i.zynq_ultra_ps_e_0.inst.fpga_soft_reset(32'h1);
            #2000 ;  // This delay depends on your clock frequency. It should be at least 16 clock cycles. 
            tb_adder2d.mpsoc_sys.design_1_i.zynq_ultra_ps_e_0.inst.por_srstb_reset(1'b1);
            tb_adder2d.mpsoc_sys.design_1_i.zynq_ultra_ps_e_0.inst.fpga_soft_reset(32'h0);
            #2000 ;  
            
            ap_start=1'b1;

            //This drives the LEDs on the GPIO output
            tb_adder2d.mpsoc_sys.design_1_i.zynq_ultra_ps_e_0.inst.write_data(32'hA0000010,16, 128'hFFFFFFF0FFFFFFF1FFFFFFF2FFFFFFF3, resp);
            #10;
            tb_adder2d.mpsoc_sys.design_1_i.zynq_ultra_ps_e_0.inst.read_data(32'hA0000020,16, read_data, resp);
            
            $stop;
       
    end

        assign temp_clk = tb_ACLK;
        assign temp_rstn = tb_ARESETn;
        
        
    design_1_wrapper mpsoc_sys
     (
     .ap_done_0(ap_done),
        .ap_idle_0(ap_idle),
        .ap_ready_0(ap_ready),
        .ap_start_0(ap_start)   
    );

      


    endmodule



* Run simulation 


.. _`fig_hls_zynq3`:

.. figure:: img/hls_zynq/hls_zynq3.jpg
    :width: 100%

    Read and write operations 