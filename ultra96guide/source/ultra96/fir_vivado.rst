FIR Filter examples in Vivado
*****************************


FIR compiler operation
======================

In this section, we will use the FIR compiler with default settings. And then write a testbench to read the data from the file and then verify the output. 

Add the FIR IP from IP catalog
------------------------------

Here, we have change the clock frequency and sample frequency to 100 MHz and rest of the settings are default settings in the FIR compiler, 


.. code-block:: text
  :linenos: 

  
  # Vivado 2019.2
  # ultra96

  create_project basic_fir ./fir_vivado/basic_fir -part xczu3eg-sbva484-1-e -force
  set_property board_part em.avnet.com:ultra96:part0:1.2 [current_project]


  # FIR compiler 
  create_ip -name fir_compiler -vendor xilinx.com -library ip -version 7.2 -module_name fir_compiler_0
  set_property -dict [list CONFIG.CoefficientVector {6,0,-4,-3,5,6,-6,-13,7,44,64,44,7,-13,-6,6,5,-3,-4,0,6} CONFIG.Sample_Frequency {100} CONFIG.Clock_Frequency {100} CONFIG.Quantization {Integer_Coefficients} CONFIG.Data_Fractional_Bits {14} CONFIG.Coefficient_Sets {1} CONFIG.Coefficient_Sign {Signed} CONFIG.Coefficient_Width {16} CONFIG.Coefficient_Fractional_Bits {0} CONFIG.Coefficient_Structure {Inferred} CONFIG.Data_Width {16} CONFIG.Output_Width {24} CONFIG.Filter_Architecture {Systolic_Multiply_Accumulate} CONFIG.ColumnConfig {11}] [get_ips fir_compiler_0]
  generate_target {instantiation_template} [get_files ./fir_vivado/basic_fir/basic_fir.srcs/sources_1/ip/fir_compiler_0/fir_compiler_0.xci]

  update_compile_order -fileset sources_1
  generate_target all [get_files  ./fir_vivado/basic_fir/basic_fir.srcs/sources_1/ip/fir_compiler_0/fir_compiler_0.xci]

  catch { config_ip_cache -export [get_ips -all fir_compiler_0] }
  export_ip_user_files -of_objects [get_files ./fir_vivado/basic_fir/basic_fir.srcs/sources_1/ip/fir_compiler_0/fir_compiler_0.xci] -no_script -sync -force -quiet
  create_ip_run [get_files -of_objects [get_fileset sources_1] ./fir_vivado/basic_fir/basic_fir.srcs/sources_1/ip/fir_compiler_0/fir_compiler_0.xci]
  launch_runs -jobs 56 fir_compiler_0_synth_1

  export_simulation -of_objects [get_files ./fir_vivado/basic_fir/basic_fir.srcs/sources_1/ip/fir_compiler_0/fir_compiler_0.xci] -directory ./fir_vivado/basic_fir/basic_fir.ip_user_files/sim_scripts -ip_user_files_dir ./fir_vivado/basic_fir/basic_fir.ip_user_files -ipstatic_source_dir ./fir_vivado/basic_fir/basic_fir.ip_user_files/ipstatic -lib_map_path [list {modelsim=./fir_vivado/basic_fir/basic_fir.cache/compile_simlib/modelsim} {questa=./fir_vivado/basic_fir/basic_fir.cache/compile_simlib/questa} {ies=./fir_vivado/basic_fir/basic_fir.cache/compile_simlib/ies} {xcelium=./fir_vivado/basic_fir/basic_fir.cache/compile_simlib/xcelium} {vcs=./fir_vivado/basic_fir/basic_fir.cache/compile_simlib/vcs} {riviera=./fir_vivado/basic_fir/basic_fir.cache/compile_simlib/riviera}] -use_ip_compiled_libs -force -quiet

  ##




Create the testbench
--------------------

.. code-block:: vhdl

    -- tb_fir.vhd 

    library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

    use std.textio.all;
    use ieee.std_logic_textio.all; -- require for writing/reading std_logic etc.

    entity tb_fir is
    end tb_fir;

    architecture tb of tb_fir is

      -----------------------------------------------------------------------
      -- Timing constants
      -----------------------------------------------------------------------
      constant CLOCK_PERIOD : time := 100 ns;
      constant T_HOLD       : time := 10 ns;
      constant T_STROBE     : time := CLOCK_PERIOD - (1 ns);

      -----------------------------------------------------------------------
      -- DUT signals
      -----------------------------------------------------------------------

      -- General signals
      signal aclk                            : std_logic := '0';  -- the master clock

      -- Data slave channel signals
      signal s_axis_data_tvalid              : std_logic := '0';  -- payload is valid
      signal s_axis_data_tready              : std_logic := '1';  -- slave is ready
      signal s_axis_data_tdata               : std_logic_vector(15 downto 0) := (others => '0');  -- data payload

      -- Data master channel signals
      signal m_axis_data_tvalid              : std_logic := '0';  -- payload is valid
      signal m_axis_data_tdata               : std_logic_vector(23 downto 0) := (others => '0');  -- data payload

      -----------------------------------------------------------------------
      -- Aliases for AXI channel TDATA and TUSER fields
      -- These are a convenience for viewing data in a simulator waveform viewer.
      -- If using ModelSim or Questa, add "-voptargs=+acc=n" to the vsim command
      -- to prevent the simulator optimizing away these signals.
      -----------------------------------------------------------------------

      -- Data slave channel alias signals
      signal s_axis_data_tdata_data        : std_logic_vector(15 downto 0) := (others => '0');

      -- Data master channel alias signals
      signal m_axis_data_tdata_data        : std_logic_vector(23 downto 0) := (others => '0');


    -- buffer for storing the text from input read-file
    file input_buf : text;  -- text is keyword
        
        
    begin
    file_open(input_buf, "impulse_data.dat",  read_mode); 

      -----------------------------------------------------------------------
      -- Instantiate the DUT
      -----------------------------------------------------------------------

      dut : entity work.fir_compiler_0
        port map (
          aclk                            => aclk,
          s_axis_data_tvalid              => s_axis_data_tvalid,
          s_axis_data_tready              => s_axis_data_tready,
          s_axis_data_tdata               => s_axis_data_tdata,
          m_axis_data_tvalid              => m_axis_data_tvalid,
          m_axis_data_tdata               => m_axis_data_tdata
          );

      -----------------------------------------------------------------------
      -- Generate clock
      -----------------------------------------------------------------------

      clock_gen : process
      begin
        aclk <= '0';
        wait for CLOCK_PERIOD;
        loop
          aclk <= '0';
          wait for CLOCK_PERIOD/2;
          aclk <= '1';
          wait for CLOCK_PERIOD/2;
        end loop;
      end process clock_gen;

      -----------------------------------------------------------------------
      -- Generate inputs
      -----------------------------------------------------------------------

      stimuli : process

        -- Procedure to drive a number of input samples with specific data
        -- data is the data value to drive on the tdata signal
        -- samples is the number of zero-data input samples to drive
        procedure drive_data ( data    : std_logic_vector(15 downto 0);
                               samples : natural := 1 ) is
          variable ip_count : integer := 0;
        begin
          ip_count := 0;
          loop
            s_axis_data_tvalid <= '1';
            s_axis_data_tdata  <= data;
            loop
              wait until rising_edge(aclk);
              exit when s_axis_data_tready = '1';
            end loop;
            ip_count := ip_count + 1;
            wait for T_HOLD;
            exit when ip_count >= samples;
          end loop;
        end procedure drive_data;


        procedure read_from_file ( samples : natural := 1 ) is
          variable data : std_logic_vector(15 downto 0);
            variable val_col1 : std_logic_vector(15 downto 0); -- to save col1 and col2 values of 1 bit
            variable read_col_from_input_buf : line;
                    variable sample_count : integer := 0;

        begin
          data := (others => '0');  -- initialize unused bits to zero
          
          readline(input_buf, read_col_from_input_buf);
          read(read_col_from_input_buf, val_col1);
          data(15 downto 0) := val_col1;      
          drive_data(data);
          
          if samples > 1 then
            
            loop 
                sample_count := sample_count + 1;
                if endfile(input_buf) then 
                    report "No data in the file" severity failure;
                end if;
                
                readline(input_buf, read_col_from_input_buf);
                read(read_col_from_input_buf, val_col1);
                data(15 downto 0) := val_col1;
                drive_data(data);
                
                exit when sample_count >= samples;

            end loop;
          end if;
        end procedure read_from_file;
        

      begin

        -- Drive inputs T_HOLD time after rising edge of clock
        wait until rising_edge(aclk);
        wait for T_HOLD;

        -- Drive the data from file
        read_from_file(150);

        

       -- End of test
        report "Not a real failure. Simulation finished successfully. Test completed successfully" severity failure;
        wait;

      end process stimuli;

      -----------------------------------------------------------------------
      -- Check outputs
      -----------------------------------------------------------------------

      check_outputs : process
        variable check_ok : boolean := true;
      begin

        -- Check outputs T_STROBE time after rising edge of clock
        wait until rising_edge(aclk);
        wait for T_STROBE;

        -- Do not check the output payload values, as this requires the behavioral model
        -- which would make this demonstration testbench unwieldy.
        -- Instead, check the protocol of the master DATA channel:
        -- check that the payload is valid (not X) when TVALID is high

        if m_axis_data_tvalid = '1' then
          if is_x(m_axis_data_tdata) then
            report "ERROR: m_axis_data_tdata is invalid when m_axis_data_tvalid is high" severity error;
            check_ok := false;
          end if;

        end if;

        assert check_ok
          report "ERROR: terminating test with failures." severity failure;

      end process check_outputs;

      -----------------------------------------------------------------------
      -- Assign TDATA / TUSER fields to aliases, for easy simulator waveform viewing
      -----------------------------------------------------------------------

      -- Data slave channel alias signals
      s_axis_data_tdata_data        <= s_axis_data_tdata(15 downto 0);

      -- Data master channel alias signals: update these only when they are valid
      m_axis_data_tdata_data        <= m_axis_data_tdata(23 downto 0) when m_axis_data_tvalid = '1';

    end tb;









Create input-data file
----------------------

Create a file with name 'impulse_data.dat' with following content. This data will be used in the testbench, 


.. code-block:: text

    0000000000000000
    0100000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0100000000000000
    0100000000000000
    0100000000000000
    0100000000000000
    0100000000000000
    0100000000000000
    0100000000000000
    0100000000000000
    0100000000000000
    0100000000000000
    0100000000000000
    0100000000000000
    0100000000000000
    0100000000000000
    0100000000000000
    0100000000000000
    0100000000000000
    0100000000000000
    0100000000000000
    0100000000000000
    0100000000000000
    0100000000000000
    0100000000000000
    0100000000000000
    0100000000000000
    0100000000000000
    0100000000000000
    0100000000000000
    0100000000000000
    0100000000000000
    0100000000000000
    0100000000000000
    0100000000000000
    0100000000000000
    0100000000000000
    0100000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000
    0000000000000000


Add testbench and input file
----------------------------

Click on '+' sign to add the testbench and impulse_data.dat file to the project as shown in :numref:`fig_fir_vivado1`. After adding the files, we will have file structure which is shown in :numerf:`fig_fir_vivado1`


.. _`fig_fir_vivado1`:

.. figure:: img/fir_vivado/fir_vivado1.jpg
    :width: 100%

    Add testbench and input data file 





Run simulation
--------------

Run the simulation now. Since input and output have binary points at 14 (with signed option), therefore set it in 'real settings' option to see the output as shown in :numref:`fig_fir_vivado2`


* First part of the output shows the coefficients of the FIR filter (as only impulse is sent). 
* Second part of the output shows the sum of the coefficients (as we are sending a series of one)

.. _`fig_fir_vivado2`:

.. figure:: img/fir_vivado/fir_vivado2.jpg
    :width: 100%

    Add testbench and input data file 



Reloadable coefficients
=======================



TCL script
----------

* Instantiate the FIR compiler with reload coefficients option, 

.. code-block:: text

    # Vivado 2019.2
    # Ultra96

    create_project fir_reload ./fir_vivado/fir_reload -part xczu3eg-sbva484-1-e
    set_property board_part em.avnet.com:ultra96:part0:1.2 [current_project]

    # FIR
    create_ip -name fir_compiler -vendor xilinx.com -library ip -version 7.2 -module_name fir_compiler_0
    set_property -dict [list CONFIG.Coefficient_Reload {true} CONFIG.Sample_Frequency {100} CONFIG.Clock_Frequency {100} CONFIG.Coefficient_Sign {Signed} CONFIG.Coefficient_Width {16} CONFIG.Coefficient_Fractional_Bits {0} CONFIG.Coefficient_Structure {Inferred} CONFIG.Data_Width {16} CONFIG.Output_Width {37} CONFIG.Filter_Architecture {Systolic_Multiply_Accumulate} CONFIG.ColumnConfig {11}] [get_ips fir_compiler_0]
    generate_target {instantiation_template} [get_files ./fir_vivado/fir_reload/fir_reload.srcs/sources_1/ip/fir_compiler_0/fir_compiler_0.xci]
    catch { config_ip_cache -export [get_ips -all fir_compiler_0] }
    export_ip_user_files -of_objects [get_files ./fir_vivado/fir_reload/fir_reload.srcs/sources_1/ip/fir_compiler_0/fir_compiler_0.xci] -no_script -sync -force -quiet
    create_ip_run [get_files -of_objects [get_fileset sources_1] ./fir_vivado/fir_reload/fir_reload.srcs/sources_1/ip/fir_compiler_0/fir_compiler_0.xci]
    launch_runs -jobs 56 fir_compiler_0_synth_1

    export_simulation -of_objects [get_files ./fir_vivado/fir_reload/fir_reload.srcs/sources_1/ip/fir_compiler_0/fir_compiler_0.xci] -directory ./fir_vivado/fir_reload/fir_reload.ip_user_files/sim_scripts -ip_user_files_dir ./fir_vivado/fir_reload/fir_reload.ip_user_files -ipstatic_source_dir ./fir_vivado/fir_reload/fir_reload.ip_user_files/ipstatic -lib_map_path [list {modelsim=./fir_vivado/fir_reload/fir_reload.cache/compile_simlib/modelsim} {questa=./fir_vivado/fir_reload/fir_reload.cache/compile_simlib/questa} {ies=./fir_vivado/fir_reload/fir_reload.cache/compile_simlib/ies} {xcelium=./fir_vivado/fir_reload/fir_reload.cache/compile_simlib/xcelium} {vcs=./fir_vivado/fir_reload/fir_reload.cache/compile_simlib/vcs} {riviera=./fir_vivado/fir_reload/fir_reload.cache/compile_simlib/riviera}] -use_ip_compiled_libs -force -quiet

    ##


Testbench
---------

* Below is the testbench, 

.. code-block:: vhdl
  :linenos: 

    -- tb_reload_fir.vhd 

    library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;
    use std.textio.all;
    use ieee.std_logic_textio.all; -- require for writing/reading std_logic etc.

    entity tb_reload_fir is
    end tb_reload_fir;

    architecture tb of tb_reload_fir is
        file input_buf : text;  -- text is keyword
      -----------------------------------------------------------------------
      -- Timing constants
      -----------------------------------------------------------------------
      constant CLOCK_PERIOD : time := 100 ns;
      constant T_HOLD       : time := 10 ns;
      constant T_STROBE     : time := CLOCK_PERIOD - (1 ns);

      -----------------------------------------------------------------------
      -- DUT signals
      -----------------------------------------------------------------------

      -- General signals
      signal aclk                            : std_logic := '0';  -- the master clock

      -- Data slave channel signals
      signal s_axis_data_tvalid              : std_logic := '0';  -- payload is valid
      signal s_axis_data_tready              : std_logic := '1';  -- slave is ready
      signal s_axis_data_tdata               : std_logic_vector(15 downto 0) := (others => '0');  -- data payload

      -- Config slave channel signals
      signal s_axis_config_tvalid            : std_logic := '0';  -- payload is valid
      signal s_axis_config_tready            : std_logic := '1';  -- slave is ready
      signal s_axis_config_tdata             : std_logic_vector(7 downto 0) := (others => '0');  -- data payload

      -- Reload slave channel signals
      signal s_axis_reload_tvalid            : std_logic := '0';  -- payload is valid
      signal s_axis_reload_tready            : std_logic := '1';  -- slave is ready
      signal s_axis_reload_tdata             : std_logic_vector(15 downto 0) := (others => '0');  -- data payload
      signal s_axis_reload_tlast             : std_logic := '0';  -- indicates end of packet

      -- Data master channel signals
      signal m_axis_data_tvalid              : std_logic := '0';  -- payload is valid
      signal m_axis_data_tdata               : std_logic_vector(39 downto 0) := (others => '0');  -- data payload

      -- Event signals
      signal event_s_reload_tlast_missing    : std_logic  :=  '0';  -- s_axis_reload_tlast low at end of reload packet
      signal event_s_reload_tlast_unexpected : std_logic  :=  '0';  -- s_axis_reload_tlast high not at end of reload packet

      -----------------------------------------------------------------------
      -- Aliases for AXI channel TDATA and TUSER fields
      -- These are a convenience for viewing data in a simulator waveform viewer.
      -- If using ModelSim or Questa, add "-voptargs=+acc=n" to the vsim command
      -- to prevent the simulator optimizing away these signals.
      -----------------------------------------------------------------------

      -- Data slave channel alias signals
      signal s_axis_data_tdata_data        : std_logic_vector(15 downto 0) := (others => '0');

      -- Config slave channel alias signals

      -- Reload slave channel alias signals
      signal s_axis_reload_tdata_coef           : std_logic_vector(15 downto 0) := (others => '0');

      -- Data master channel alias signals
      signal m_axis_data_tdata_data        : std_logic_vector(36 downto 0) := (others => '0');


    begin

      -----------------------------------------------------------------------
      -- Instantiate the DUT
      -----------------------------------------------------------------------

      dut : entity work.fir_compiler_0
        port map (
          aclk                            => aclk,
          s_axis_data_tvalid              => s_axis_data_tvalid,
          s_axis_data_tready              => s_axis_data_tready,
          s_axis_data_tdata               => s_axis_data_tdata,
          s_axis_config_tvalid            => s_axis_config_tvalid,
          s_axis_config_tready            => s_axis_config_tready,
          s_axis_config_tdata             => s_axis_config_tdata,
          s_axis_reload_tvalid            => s_axis_reload_tvalid,
          s_axis_reload_tready            => s_axis_reload_tready,
          s_axis_reload_tdata             => s_axis_reload_tdata,
          s_axis_reload_tlast             => s_axis_reload_tlast,
          m_axis_data_tvalid              => m_axis_data_tvalid,
          m_axis_data_tdata               => m_axis_data_tdata,
          event_s_reload_tlast_missing    => event_s_reload_tlast_missing,
          event_s_reload_tlast_unexpected => event_s_reload_tlast_unexpected
          );

      -----------------------------------------------------------------------
      -- Generate clock
      -----------------------------------------------------------------------

      clock_gen : process
      begin
        aclk <= '0';
        wait for CLOCK_PERIOD;
        loop
          aclk <= '0';
          wait for CLOCK_PERIOD/2;
          aclk <= '1';
          wait for CLOCK_PERIOD/2;
        end loop;
      end process clock_gen;

      -----------------------------------------------------------------------
      -- Generate inputs
      -----------------------------------------------------------------------

      stimuli : process


        
        variable val_col1 : integer;
        -- Procedure to drive a number of input samples with specific data
        -- data is the data value to drive on the tdata signal
        -- samples is the number of zero-data input samples to drive
        procedure drive_data ( data    : std_logic_vector(15 downto 0);
                               samples : natural := 1 ) is
          variable ip_count : integer := 0;
        begin
          ip_count := 0;
          loop
            s_axis_data_tvalid <= '1';
            s_axis_data_tdata  <= data;
            loop
              wait until rising_edge(aclk);
              exit when s_axis_data_tready = '1';
            end loop;
            ip_count := ip_count + 1;
            wait for T_HOLD;
            exit when ip_count >= samples;
          end loop;
        end procedure drive_data;

        -- Procedure to drive a number of zero-data input samples
        -- samples is the number of zero-data input samples to drive
        procedure drive_zeros ( samples : natural := 1 ) is
        begin
          drive_data((others => '0'), samples);
        end procedure drive_zeros;

        -- Procedure to drive an impulse and let the impulse response emerge on the data master channel
        -- samples is the number of input samples to drive; default is enough for impulse response output to emerge
        procedure drive_impulse ( samples : natural := 48 ) is
          variable impulse : std_logic_vector(15 downto 0);
        begin
          impulse := (others => '0');  -- initialize unused bits to zero
          impulse(15 downto 0) := "0000000000000001";
          drive_data(impulse);
          if samples > 1 then
            drive_zeros(samples-1);
          end if;
        end procedure drive_impulse;

        variable read_col_from_input_buf : line; -- read lines one by one from input_buf
      begin
        file_open(input_buf, "reload_coef.dat",  read_mode);

        -- Drive inputs T_HOLD time after rising edge of clock
        wait until rising_edge(aclk);
        wait for T_HOLD;

        -- Drive a single impulse and let the impulse response emerge
        drive_impulse;

        -- Drive another impulse, during which demonstrate use and effect of AXI handshaking signals
        drive_impulse(2);  -- start of impulse; data is now zero
        s_axis_data_tvalid <= '0';
        wait for CLOCK_PERIOD * 5;  -- provide no data for 5 input samples worth
        drive_zeros(46);  -- back to normal operation

        -- Load a new filter coefficient set using the reload slave channel
        s_axis_data_tvalid   <= '0';
        s_axis_reload_tvalid <= '1';
        s_axis_reload_tdata  <= (others => '0');  -- clear unused bits of TDATA
        -- The new coefficients are loaded serially, over 11 transactions.
        -- Load a constant value for each coefficient.
        -- For more information about reload packet order and length, please see the FIR Compiler Datasheet.
        for coef in 0 to 10 loop
          s_axis_reload_tvalid <= '1';
          s_axis_reload_tdata <= (others => '0');  -- clear unused bits of TDATA

         -- read data from file 
         readline(input_buf, read_col_from_input_buf);
         read(read_col_from_input_buf, val_col1);
         s_axis_reload_tdata(15 downto 0) <= std_logic_vector(to_signed(val_col1, 16));

          if coef = 10 then
            s_axis_reload_tlast <= '1';  -- signal last transaction in reload packet
          else
            s_axis_reload_tlast <= '0';
          end if;
          loop
            wait until rising_edge(aclk);
            exit when s_axis_reload_tready = '1';
          end loop;
          wait for T_HOLD;
        end loop;
        s_axis_reload_tlast  <= '0';
        s_axis_reload_tvalid <= '0';

        -- A packet on the config slave channel signals that the new coefficients should now be used.
        -- The config packet is required only for signalling: its data is irrelevant.
        s_axis_config_tvalid <= '1';
        s_axis_config_tdata  <= (others => '0');  -- don't care about TDATA - it is unused
        loop
          wait until rising_edge(aclk);
          exit when s_axis_config_tready = '1';
        end loop;
        wait for T_HOLD;
        s_axis_config_tvalid <= '0';

        -- The loaded filter coefficient set has now been enabled.
        -- Drive an impulse to show the new configuration.
        drive_impulse(96);  -- drive for long enough to be sure to get full impulse response out

        -- End of test
        report "Not a real failure. Simulation finished successfully. Test completed successfully" severity failure;
        wait;

      end process stimuli;

      -----------------------------------------------------------------------
      -- Check outputs
      -----------------------------------------------------------------------

      check_outputs : process
        variable check_ok : boolean := true;
      begin

        -- Check outputs T_STROBE time after rising edge of clock
        wait until rising_edge(aclk);
        wait for T_STROBE;

        -- Do not check the output payload values, as this requires the behavioral model
        -- which would make this demonstration testbench unwieldy.
        -- Instead, check the protocol of the master DATA channel:
        -- check that the payload is valid (not X) when TVALID is high

        if m_axis_data_tvalid = '1' then
          if is_x(m_axis_data_tdata) then
            report "ERROR: m_axis_data_tdata is invalid when m_axis_data_tvalid is high" severity error;
            check_ok := false;
          end if;

        end if;

        assert check_ok
          report "ERROR: terminating test with failures." severity failure;

      end process check_outputs;

      -----------------------------------------------------------------------
      -- Assign TDATA / TUSER fields to aliases, for easy simulator waveform viewing
      -----------------------------------------------------------------------

      -- Data slave channel alias signals
      s_axis_data_tdata_data        <= s_axis_data_tdata(15 downto 0);

      -- Config slave channel alias signals

      -- Reload slave channel alias signals
      s_axis_reload_tdata_coef          <= s_axis_reload_tdata(15 downto 0);

      -- Data master channel alias signals: update these only when they are valid
      m_axis_data_tdata_data        <= m_axis_data_tdata(36 downto 0) when m_axis_data_tvalid = '1';

    end tb;


* Since, while instantiating the FIR compiler, we have used 21 symmetric coefficients with 'inferred' option (see :numref:`fig_fir_vivado4`), therefore only 11 coefficients can be reloaded. 

.. _`fig_fir_vivado4`:

.. figure:: img/fir_vivado/fir_vivado4.jpg
    :width: 100%

    Inferred option is used here, therefore only half of the coefficient will be loaded


Below are the values of reload-coefficents which are saved in the file "reload_coef.dat". 

.. code-block:: text

    16
    10
    -14
    -13
    15
    16
    -16
    -113
    17
    144
    164


Simulation results
------------------

Run the simulation to see the results in :numref:`fig_fir_vivado3`

.. _`fig_fir_vivado3`:

.. figure:: img/fir_vivado/fir_vivado3.jpg
    :width: 100%

    Simulation results for 'reload coefficients'
