Message extractor
*****************


Testbench to test the input data
================================



.. code-block:: VHDL

    -- mes_ext_tb.vhd

    library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

    entity mes_ext_tb is
    end entity ; -- mes_ext


    architecture tb of mes_ext_tb is
        -- Time period for clock
        constant T : time := 20 ns; 

        -- input/output signals for 'mes_ext.vhd'
        signal clk, reset_n : std_logic;
        signal in_valid : std_logic;
        signal in_start_of_packet, in_end_of_packet : std_logic; 
        signal in_data  : unsigned(63 downto 0);
        signal in_empty : unsigned(2 downto 0);
        signal in_error : std_logic;
        signal ready : std_logic; 

        signal out_valid : std_logic; 
        signal out_data : unsigned(255 downto 0);
        signal out_byte_mask : unsigned(31 downto 0);
        -- signal i, i_next : natural := 0;

        -- declare record type
        -- test-vectors are added in the code (not in the file to avoid the use of 
        -- 'modelsim  project', as it will add addition files for emailing the solution)
        type test_vector is record
            in_data  : unsigned(63 downto 0);
            in_start_of_packet, in_end_of_packet : std_logic; 
            in_valid : std_logic;
            in_empty : unsigned(2 downto 0);
            in_error : std_logic;
        end record; 

        -- test-vector provided in the assignment
        type test_vector_array is array (natural range <>) of test_vector;
        constant test_vectors : test_vector_array := (
            -- in_data, in_start_of_packet, in_end_of_packet, in_valid, in_empty, in_error,
            (x"0008000862626262",'1','0','1',"000",'0'),
            (x"62626262000c6868",'0','0','1',"000",'0'),
            (x"6868686868686868",'0','0','1',"000",'0'),
            (x"6868000a70707070",'0','0','1',"000",'0'),
            (x"707070707070000f",'0','0','1',"000",'0'),
            (x"7a7a7a7a7a7a7a7a",'0','0','1',"000",'0'),
            (x"7a7a7a7a7a7a7a00",'0','0','1',"000",'0'),
            (x"0e4d4d4d4d4d4d4d",'0','0','1',"000",'0'),
            (x"4d4d4d4d4d4d4d00",'0','0','1',"000",'0'),
            (x"1138383838383838",'0','0','1',"000",'0'),
            (x"3838383838383838",'0','0','1',"000",'0'),
            (x"3838000b31313131",'0','0','1',"000",'0'),
            (x"3131313131313100",'0','0','1',"000",'0'),
            (x"095a5a5a5a5a5a5a",'0','0','1',"000",'0'),
            (x"0000000000005a5a",'0','1','1',"110",'0'),
            (x"0008000862626262",'1','0','1',"000",'0')  -- newly added
        );


    begin     

        -- unit under test
        UUT : entity work.mes_ext port map (
            clk => clk, 
            reset_n => reset_n,
            in_valid => in_valid, 
            in_start_of_packet => in_start_of_packet,
            in_end_of_packet => in_end_of_packet,
            in_data => in_data, 
            in_empty => in_empty,
            in_error => in_error,
            out_ready  => ready, 
            out_valid => out_valid,
            out_data => out_data,
            out_byte_mask => out_byte_mask 
        );
        
        -- continuous clock
        process begin
            clk <= '1';
            wait for T/2;
            clk <= '0';
            wait for T/2;
        end process;


        reset_n <= '0', '1' after T;

        -- read input values from test-vector
        process 
          variable i, i_next : natural := 0;
        begin
            -- i <= i_next;
            i := i_next;
            -- if ready = '1' then 
            --    i_next <= i + 1;
                 i_next := i + 1;
                in_valid <= test_vectors(i).in_valid;  
                in_start_of_packet <= test_vectors(i).in_start_of_packet;
                in_end_of_packet <= test_vectors(i).in_end_of_packet;
                in_data <= test_vectors(i).in_data;
                in_empty <= test_vectors(i).in_empty;
                in_error <= test_vectors(i).in_error;
             -- end if;

            wait for T;
        end process; 

    end tb ;







.. code-block:: vhdl

    -- mes_ext.vhd


    library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

    entity mes_ext is

    port(
        clk, reset_n : in std_logic;
        in_valid : in std_logic;
        in_start_of_packet, in_end_of_packet : in std_logic;
        in_data : in unsigned(63 downto 0);
        in_empty : in unsigned(2 downto 0);
        in_error : in std_logic;
        
        out_ready : out std_logic;
        out_valid : out std_logic;
        out_data : out unsigned(255 downto 0);
        out_byte_mask : out unsigned(31 downto 0)
    );

    end entity;

    architecture arch of mes_ext is
    begin

    end arch; 





Binary counter
==============


.. code-block:: vhdl

    -- binaryCounter.vhd
    
    library ieee; 
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

    entity binaryCounter is
        generic (
                N : integer := 3    -- N-bit binary counter
        );
        
        port(
                clk, reset : in std_logic;
                complete_tick : out std_logic; 
                count : out std_logic_vector(N-1 downto 0)
        );
    end binaryCounter;


    architecture arch of binaryCounter is
        constant MAX_COUNT : integer := 2**N - 1; -- maximum count for N bit
        signal count_reg, count_next : unsigned(N-1 downto 0);
    begin
        process(clk, reset)
        begin
            if reset = '1' then 
                count_reg <= (others=>'0');  -- set count to 0 if reset
            elsif   clk'event and clk='1' then
                count_reg <= count_next;  -- assign next value of count
            else  -- note that else block is not required
                null;
            end if;
        end process;
        
        count_next <= count_reg+1;  -- increase the count
        
        -- Generate 'tick' on each maximum count
        complete_tick <= '1' when count_reg = MAX_COUNT else '0';  
        
        count <= std_logic_vector(count_reg);  -- assign value to output port
    end arch;



